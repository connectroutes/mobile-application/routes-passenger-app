///
//  Generated code. Do not modify.
//  source: payment.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import 'payment.pbenum.dart';

export 'payment.pbenum.dart';

class PaymentOption extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('PaymentOption', package: const $pb.PackageName('com.connectroutes.payment'), createEmptyInstance: create)
    ..e<PaymentMethod>(1, 'method', $pb.PbFieldType.OE, defaultOrMaker: PaymentMethod.cash, valueOf: PaymentMethod.valueOf, enumValues: PaymentMethod.values)
    ..aOS(2, 'id')
    ..e<CardType>(3, 'cardType', $pb.PbFieldType.OE, protoName: 'cardType', defaultOrMaker: CardType.visa, valueOf: CardType.valueOf, enumValues: CardType.values)
    ..aOS(4, 'cardLast4', protoName: 'cardLast4')
    ..aOS(5, 'bin')
    ..aOS(6, 'expiryMonth', protoName: 'expiryMonth')
    ..aOS(7, 'expiryYear', protoName: 'expiryYear')
    ..aInt64(8, 'lastUsed', protoName: 'lastUsed')
    ..hasRequiredFields = false
  ;

  PaymentOption._() : super();
  factory PaymentOption() => create();
  factory PaymentOption.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PaymentOption.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  PaymentOption clone() => PaymentOption()..mergeFromMessage(this);
  PaymentOption copyWith(void Function(PaymentOption) updates) => super.copyWith((message) => updates(message as PaymentOption));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PaymentOption create() => PaymentOption._();
  PaymentOption createEmptyInstance() => create();
  static $pb.PbList<PaymentOption> createRepeated() => $pb.PbList<PaymentOption>();
  @$core.pragma('dart2js:noInline')
  static PaymentOption getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PaymentOption>(create);
  static PaymentOption _defaultInstance;

  @$pb.TagNumber(1)
  PaymentMethod get method => $_getN(0);
  @$pb.TagNumber(1)
  set method(PaymentMethod v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasMethod() => $_has(0);
  @$pb.TagNumber(1)
  void clearMethod() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get id => $_getSZ(1);
  @$pb.TagNumber(2)
  set id($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasId() => $_has(1);
  @$pb.TagNumber(2)
  void clearId() => clearField(2);

  @$pb.TagNumber(3)
  CardType get cardType => $_getN(2);
  @$pb.TagNumber(3)
  set cardType(CardType v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasCardType() => $_has(2);
  @$pb.TagNumber(3)
  void clearCardType() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get cardLast4 => $_getSZ(3);
  @$pb.TagNumber(4)
  set cardLast4($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasCardLast4() => $_has(3);
  @$pb.TagNumber(4)
  void clearCardLast4() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get bin => $_getSZ(4);
  @$pb.TagNumber(5)
  set bin($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasBin() => $_has(4);
  @$pb.TagNumber(5)
  void clearBin() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get expiryMonth => $_getSZ(5);
  @$pb.TagNumber(6)
  set expiryMonth($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasExpiryMonth() => $_has(5);
  @$pb.TagNumber(6)
  void clearExpiryMonth() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get expiryYear => $_getSZ(6);
  @$pb.TagNumber(7)
  set expiryYear($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasExpiryYear() => $_has(6);
  @$pb.TagNumber(7)
  void clearExpiryYear() => clearField(7);

  @$pb.TagNumber(8)
  $fixnum.Int64 get lastUsed => $_getI64(7);
  @$pb.TagNumber(8)
  set lastUsed($fixnum.Int64 v) { $_setInt64(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasLastUsed() => $_has(7);
  @$pb.TagNumber(8)
  void clearLastUsed() => clearField(8);
}

class GetPaymentOptionsRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('GetPaymentOptionsRequest', package: const $pb.PackageName('com.connectroutes.payment'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  GetPaymentOptionsRequest._() : super();
  factory GetPaymentOptionsRequest() => create();
  factory GetPaymentOptionsRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetPaymentOptionsRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  GetPaymentOptionsRequest clone() => GetPaymentOptionsRequest()..mergeFromMessage(this);
  GetPaymentOptionsRequest copyWith(void Function(GetPaymentOptionsRequest) updates) => super.copyWith((message) => updates(message as GetPaymentOptionsRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetPaymentOptionsRequest create() => GetPaymentOptionsRequest._();
  GetPaymentOptionsRequest createEmptyInstance() => create();
  static $pb.PbList<GetPaymentOptionsRequest> createRepeated() => $pb.PbList<GetPaymentOptionsRequest>();
  @$core.pragma('dart2js:noInline')
  static GetPaymentOptionsRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetPaymentOptionsRequest>(create);
  static GetPaymentOptionsRequest _defaultInstance;
}

class GetPaymentOptionsResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('GetPaymentOptionsResponse', package: const $pb.PackageName('com.connectroutes.payment'), createEmptyInstance: create)
    ..pc<PaymentOption>(1, 'options', $pb.PbFieldType.PM, subBuilder: PaymentOption.create)
    ..hasRequiredFields = false
  ;

  GetPaymentOptionsResponse._() : super();
  factory GetPaymentOptionsResponse() => create();
  factory GetPaymentOptionsResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetPaymentOptionsResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  GetPaymentOptionsResponse clone() => GetPaymentOptionsResponse()..mergeFromMessage(this);
  GetPaymentOptionsResponse copyWith(void Function(GetPaymentOptionsResponse) updates) => super.copyWith((message) => updates(message as GetPaymentOptionsResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetPaymentOptionsResponse create() => GetPaymentOptionsResponse._();
  GetPaymentOptionsResponse createEmptyInstance() => create();
  static $pb.PbList<GetPaymentOptionsResponse> createRepeated() => $pb.PbList<GetPaymentOptionsResponse>();
  @$core.pragma('dart2js:noInline')
  static GetPaymentOptionsResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetPaymentOptionsResponse>(create);
  static GetPaymentOptionsResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<PaymentOption> get options => $_getList(0);
}

class AddCardRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AddCardRequest', package: const $pb.PackageName('com.connectroutes.payment'), createEmptyInstance: create)
    ..aOS(1, 'cardNo', protoName: 'cardNo')
    ..aOS(2, 'cardExpiryMonth', protoName: 'cardExpiryMonth')
    ..aOS(3, 'cardExpiryYear', protoName: 'cardExpiryYear')
    ..aOS(4, 'cardCvv', protoName: 'cardCvv')
    ..aOS(5, 'cardPin', protoName: 'cardPin')
    ..hasRequiredFields = false
  ;

  AddCardRequest._() : super();
  factory AddCardRequest() => create();
  factory AddCardRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AddCardRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  AddCardRequest clone() => AddCardRequest()..mergeFromMessage(this);
  AddCardRequest copyWith(void Function(AddCardRequest) updates) => super.copyWith((message) => updates(message as AddCardRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AddCardRequest create() => AddCardRequest._();
  AddCardRequest createEmptyInstance() => create();
  static $pb.PbList<AddCardRequest> createRepeated() => $pb.PbList<AddCardRequest>();
  @$core.pragma('dart2js:noInline')
  static AddCardRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AddCardRequest>(create);
  static AddCardRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get cardNo => $_getSZ(0);
  @$pb.TagNumber(1)
  set cardNo($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasCardNo() => $_has(0);
  @$pb.TagNumber(1)
  void clearCardNo() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get cardExpiryMonth => $_getSZ(1);
  @$pb.TagNumber(2)
  set cardExpiryMonth($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasCardExpiryMonth() => $_has(1);
  @$pb.TagNumber(2)
  void clearCardExpiryMonth() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get cardExpiryYear => $_getSZ(2);
  @$pb.TagNumber(3)
  set cardExpiryYear($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasCardExpiryYear() => $_has(2);
  @$pb.TagNumber(3)
  void clearCardExpiryYear() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get cardCvv => $_getSZ(3);
  @$pb.TagNumber(4)
  set cardCvv($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasCardCvv() => $_has(3);
  @$pb.TagNumber(4)
  void clearCardCvv() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get cardPin => $_getSZ(4);
  @$pb.TagNumber(5)
  set cardPin($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasCardPin() => $_has(4);
  @$pb.TagNumber(5)
  void clearCardPin() => clearField(5);
}

class AddCardResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AddCardResponse', package: const $pb.PackageName('com.connectroutes.payment'), createEmptyInstance: create)
    ..aOS(1, 'message')
    ..aOS(2, 'action')
    ..aOS(3, 'actionUrl')
    ..aOS(4, 'reference')
    ..aOM<PaymentOption>(5, 'card', subBuilder: PaymentOption.create)
    ..hasRequiredFields = false
  ;

  AddCardResponse._() : super();
  factory AddCardResponse() => create();
  factory AddCardResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AddCardResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  AddCardResponse clone() => AddCardResponse()..mergeFromMessage(this);
  AddCardResponse copyWith(void Function(AddCardResponse) updates) => super.copyWith((message) => updates(message as AddCardResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AddCardResponse create() => AddCardResponse._();
  AddCardResponse createEmptyInstance() => create();
  static $pb.PbList<AddCardResponse> createRepeated() => $pb.PbList<AddCardResponse>();
  @$core.pragma('dart2js:noInline')
  static AddCardResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AddCardResponse>(create);
  static AddCardResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get action => $_getSZ(1);
  @$pb.TagNumber(2)
  set action($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasAction() => $_has(1);
  @$pb.TagNumber(2)
  void clearAction() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get actionUrl => $_getSZ(2);
  @$pb.TagNumber(3)
  set actionUrl($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasActionUrl() => $_has(2);
  @$pb.TagNumber(3)
  void clearActionUrl() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get reference => $_getSZ(3);
  @$pb.TagNumber(4)
  set reference($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasReference() => $_has(3);
  @$pb.TagNumber(4)
  void clearReference() => clearField(4);

  @$pb.TagNumber(5)
  PaymentOption get card => $_getN(4);
  @$pb.TagNumber(5)
  set card(PaymentOption v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasCard() => $_has(4);
  @$pb.TagNumber(5)
  void clearCard() => clearField(5);
  @$pb.TagNumber(5)
  PaymentOption ensureCard() => $_ensure(4);
}

class SubmitCardAuthorizationRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('SubmitCardAuthorizationRequest', package: const $pb.PackageName('com.connectroutes.payment'), createEmptyInstance: create)
    ..aOS(1, 'action')
    ..aOS(2, 'value')
    ..aOS(3, 'reference')
    ..hasRequiredFields = false
  ;

  SubmitCardAuthorizationRequest._() : super();
  factory SubmitCardAuthorizationRequest() => create();
  factory SubmitCardAuthorizationRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SubmitCardAuthorizationRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  SubmitCardAuthorizationRequest clone() => SubmitCardAuthorizationRequest()..mergeFromMessage(this);
  SubmitCardAuthorizationRequest copyWith(void Function(SubmitCardAuthorizationRequest) updates) => super.copyWith((message) => updates(message as SubmitCardAuthorizationRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SubmitCardAuthorizationRequest create() => SubmitCardAuthorizationRequest._();
  SubmitCardAuthorizationRequest createEmptyInstance() => create();
  static $pb.PbList<SubmitCardAuthorizationRequest> createRepeated() => $pb.PbList<SubmitCardAuthorizationRequest>();
  @$core.pragma('dart2js:noInline')
  static SubmitCardAuthorizationRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SubmitCardAuthorizationRequest>(create);
  static SubmitCardAuthorizationRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get action => $_getSZ(0);
  @$pb.TagNumber(1)
  set action($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasAction() => $_has(0);
  @$pb.TagNumber(1)
  void clearAction() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get value => $_getSZ(1);
  @$pb.TagNumber(2)
  set value($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasValue() => $_has(1);
  @$pb.TagNumber(2)
  void clearValue() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get reference => $_getSZ(2);
  @$pb.TagNumber(3)
  set reference($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasReference() => $_has(2);
  @$pb.TagNumber(3)
  void clearReference() => clearField(3);
}

class AddCardByReferenceRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AddCardByReferenceRequest', package: const $pb.PackageName('com.connectroutes.payment'), createEmptyInstance: create)
    ..aOS(1, 'reference')
    ..hasRequiredFields = false
  ;

  AddCardByReferenceRequest._() : super();
  factory AddCardByReferenceRequest() => create();
  factory AddCardByReferenceRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AddCardByReferenceRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  AddCardByReferenceRequest clone() => AddCardByReferenceRequest()..mergeFromMessage(this);
  AddCardByReferenceRequest copyWith(void Function(AddCardByReferenceRequest) updates) => super.copyWith((message) => updates(message as AddCardByReferenceRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AddCardByReferenceRequest create() => AddCardByReferenceRequest._();
  AddCardByReferenceRequest createEmptyInstance() => create();
  static $pb.PbList<AddCardByReferenceRequest> createRepeated() => $pb.PbList<AddCardByReferenceRequest>();
  @$core.pragma('dart2js:noInline')
  static AddCardByReferenceRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AddCardByReferenceRequest>(create);
  static AddCardByReferenceRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get reference => $_getSZ(0);
  @$pb.TagNumber(1)
  set reference($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasReference() => $_has(0);
  @$pb.TagNumber(1)
  void clearReference() => clearField(1);
}

