///
//  Generated code. Do not modify.
//  source: authentication.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'authentication.pb.dart' as $0;
export 'authentication.pb.dart';

class AuthenticationServiceClient extends $grpc.Client {
  static final _$requestOtp =
      $grpc.ClientMethod<$0.RequestOtpRequest, $0.RequestOtpResponse>(
          '/com.connectroutes.authentication.AuthenticationService/RequestOtp',
          ($0.RequestOtpRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.RequestOtpResponse.fromBuffer(value));
  static final _$submitOtp =
      $grpc.ClientMethod<$0.SubmitOtpRequest, $0.SubmitOtpResponse>(
          '/com.connectroutes.authentication.AuthenticationService/SubmitOtp',
          ($0.SubmitOtpRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.SubmitOtpResponse.fromBuffer(value));
  static final _$submitPersonalInfo = $grpc.ClientMethod<
          $0.SubmitPersonalInfoRequest, $0.SubmitPersonalInfoResponse>(
      '/com.connectroutes.authentication.AuthenticationService/SubmitPersonalInfo',
      ($0.SubmitPersonalInfoRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.SubmitPersonalInfoResponse.fromBuffer(value));

  AuthenticationServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions options})
      : super(channel, options: options);

  $grpc.ResponseFuture<$0.RequestOtpResponse> requestOtp(
      $0.RequestOtpRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$requestOtp, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.SubmitOtpResponse> submitOtp(
      $0.SubmitOtpRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$submitOtp, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.SubmitPersonalInfoResponse> submitPersonalInfo(
      $0.SubmitPersonalInfoRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$submitPersonalInfo, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }
}

abstract class AuthenticationServiceBase extends $grpc.Service {
  $core.String get $name =>
      'com.connectroutes.authentication.AuthenticationService';

  AuthenticationServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.RequestOtpRequest, $0.RequestOtpResponse>(
        'RequestOtp',
        requestOtp_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.RequestOtpRequest.fromBuffer(value),
        ($0.RequestOtpResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SubmitOtpRequest, $0.SubmitOtpResponse>(
        'SubmitOtp',
        submitOtp_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.SubmitOtpRequest.fromBuffer(value),
        ($0.SubmitOtpResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SubmitPersonalInfoRequest,
            $0.SubmitPersonalInfoResponse>(
        'SubmitPersonalInfo',
        submitPersonalInfo_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.SubmitPersonalInfoRequest.fromBuffer(value),
        ($0.SubmitPersonalInfoResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.RequestOtpResponse> requestOtp_Pre($grpc.ServiceCall call,
      $async.Future<$0.RequestOtpRequest> request) async {
    return requestOtp(call, await request);
  }

  $async.Future<$0.SubmitOtpResponse> submitOtp_Pre($grpc.ServiceCall call,
      $async.Future<$0.SubmitOtpRequest> request) async {
    return submitOtp(call, await request);
  }

  $async.Future<$0.SubmitPersonalInfoResponse> submitPersonalInfo_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.SubmitPersonalInfoRequest> request) async {
    return submitPersonalInfo(call, await request);
  }

  $async.Future<$0.RequestOtpResponse> requestOtp(
      $grpc.ServiceCall call, $0.RequestOtpRequest request);
  $async.Future<$0.SubmitOtpResponse> submitOtp(
      $grpc.ServiceCall call, $0.SubmitOtpRequest request);
  $async.Future<$0.SubmitPersonalInfoResponse> submitPersonalInfo(
      $grpc.ServiceCall call, $0.SubmitPersonalInfoRequest request);
}
