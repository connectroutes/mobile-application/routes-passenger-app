///
//  Generated code. Do not modify.
//  source: payment.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

// ignore_for_file: UNDEFINED_SHOWN_NAME,UNUSED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class PaymentMethod extends $pb.ProtobufEnum {
  static const PaymentMethod cash = PaymentMethod._(0, 'cash');
  static const PaymentMethod card = PaymentMethod._(1, 'card');

  static const $core.List<PaymentMethod> values = <PaymentMethod> [
    cash,
    card,
  ];

  static final $core.Map<$core.int, PaymentMethod> _byValue = $pb.ProtobufEnum.initByValue(values);
  static PaymentMethod valueOf($core.int value) => _byValue[value];

  const PaymentMethod._($core.int v, $core.String n) : super(v, n);
}

class CardType extends $pb.ProtobufEnum {
  static const CardType visa = CardType._(0, 'visa');
  static const CardType mastercard = CardType._(1, 'mastercard');

  static const $core.List<CardType> values = <CardType> [
    visa,
    mastercard,
  ];

  static final $core.Map<$core.int, CardType> _byValue = $pb.ProtobufEnum.initByValue(values);
  static CardType valueOf($core.int value) => _byValue[value];

  const CardType._($core.int v, $core.String n) : super(v, n);
}

