///
//  Generated code. Do not modify.
//  source: trip.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import 'trip.pbenum.dart';

export 'trip.pbenum.dart';

class Address extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Address', package: const $pb.PackageName('com.connectroutes.trip'), createEmptyInstance: create)
    ..aOS(1, 'description')
    ..aOS(2, 'placeId')
    ..hasRequiredFields = false
  ;

  Address._() : super();
  factory Address() => create();
  factory Address.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Address.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Address clone() => Address()..mergeFromMessage(this);
  Address copyWith(void Function(Address) updates) => super.copyWith((message) => updates(message as Address));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Address create() => Address._();
  Address createEmptyInstance() => create();
  static $pb.PbList<Address> createRepeated() => $pb.PbList<Address>();
  @$core.pragma('dart2js:noInline')
  static Address getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Address>(create);
  static Address _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get description => $_getSZ(0);
  @$pb.TagNumber(1)
  set description($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasDescription() => $_has(0);
  @$pb.TagNumber(1)
  void clearDescription() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get placeId => $_getSZ(1);
  @$pb.TagNumber(2)
  set placeId($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPlaceId() => $_has(1);
  @$pb.TagNumber(2)
  void clearPlaceId() => clearField(2);
}

class Amount extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Amount', package: const $pb.PackageName('com.connectroutes.trip'), createEmptyInstance: create)
    ..aOS(1, 'currency')
    ..a<$core.double>(2, 'amount', $pb.PbFieldType.OD)
    ..hasRequiredFields = false
  ;

  Amount._() : super();
  factory Amount() => create();
  factory Amount.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Amount.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Amount clone() => Amount()..mergeFromMessage(this);
  Amount copyWith(void Function(Amount) updates) => super.copyWith((message) => updates(message as Amount));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Amount create() => Amount._();
  Amount createEmptyInstance() => create();
  static $pb.PbList<Amount> createRepeated() => $pb.PbList<Amount>();
  @$core.pragma('dart2js:noInline')
  static Amount getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Amount>(create);
  static Amount _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get currency => $_getSZ(0);
  @$pb.TagNumber(1)
  set currency($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasCurrency() => $_has(0);
  @$pb.TagNumber(1)
  void clearCurrency() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get amount => $_getN(1);
  @$pb.TagNumber(2)
  set amount($core.double v) { $_setDouble(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasAmount() => $_has(1);
  @$pb.TagNumber(2)
  void clearAmount() => clearField(2);
}

class Trip extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Trip', package: const $pb.PackageName('com.connectroutes.trip'), createEmptyInstance: create)
    ..aOS(1, 'id')
    ..aOM<Address>(2, 'pickupAddress', subBuilder: Address.create)
    ..aOM<Address>(3, 'destinationAddress', subBuilder: Address.create)
    ..e<TripStatus>(4, 'status', $pb.PbFieldType.OE, defaultOrMaker: TripStatus.created, valueOf: TripStatus.valueOf, enumValues: TripStatus.values)
    ..aInt64(5, 'requestTime')
    ..aInt64(6, 'pickupTime')
    ..aInt64(7, 'endTime')
    ..aOM<Driver>(8, 'driver', subBuilder: Driver.create)
    ..aOM<Vehicle>(9, 'vehicle', subBuilder: Vehicle.create)
    ..aOS(10, 'invoiceId')
    ..aOM<Amount>(11, 'cost', subBuilder: Amount.create)
    ..hasRequiredFields = false
  ;

  Trip._() : super();
  factory Trip() => create();
  factory Trip.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Trip.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Trip clone() => Trip()..mergeFromMessage(this);
  Trip copyWith(void Function(Trip) updates) => super.copyWith((message) => updates(message as Trip));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Trip create() => Trip._();
  Trip createEmptyInstance() => create();
  static $pb.PbList<Trip> createRepeated() => $pb.PbList<Trip>();
  @$core.pragma('dart2js:noInline')
  static Trip getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Trip>(create);
  static Trip _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  Address get pickupAddress => $_getN(1);
  @$pb.TagNumber(2)
  set pickupAddress(Address v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasPickupAddress() => $_has(1);
  @$pb.TagNumber(2)
  void clearPickupAddress() => clearField(2);
  @$pb.TagNumber(2)
  Address ensurePickupAddress() => $_ensure(1);

  @$pb.TagNumber(3)
  Address get destinationAddress => $_getN(2);
  @$pb.TagNumber(3)
  set destinationAddress(Address v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasDestinationAddress() => $_has(2);
  @$pb.TagNumber(3)
  void clearDestinationAddress() => clearField(3);
  @$pb.TagNumber(3)
  Address ensureDestinationAddress() => $_ensure(2);

  @$pb.TagNumber(4)
  TripStatus get status => $_getN(3);
  @$pb.TagNumber(4)
  set status(TripStatus v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasStatus() => $_has(3);
  @$pb.TagNumber(4)
  void clearStatus() => clearField(4);

  @$pb.TagNumber(5)
  $fixnum.Int64 get requestTime => $_getI64(4);
  @$pb.TagNumber(5)
  set requestTime($fixnum.Int64 v) { $_setInt64(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasRequestTime() => $_has(4);
  @$pb.TagNumber(5)
  void clearRequestTime() => clearField(5);

  @$pb.TagNumber(6)
  $fixnum.Int64 get pickupTime => $_getI64(5);
  @$pb.TagNumber(6)
  set pickupTime($fixnum.Int64 v) { $_setInt64(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasPickupTime() => $_has(5);
  @$pb.TagNumber(6)
  void clearPickupTime() => clearField(6);

  @$pb.TagNumber(7)
  $fixnum.Int64 get endTime => $_getI64(6);
  @$pb.TagNumber(7)
  set endTime($fixnum.Int64 v) { $_setInt64(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasEndTime() => $_has(6);
  @$pb.TagNumber(7)
  void clearEndTime() => clearField(7);

  @$pb.TagNumber(8)
  Driver get driver => $_getN(7);
  @$pb.TagNumber(8)
  set driver(Driver v) { setField(8, v); }
  @$pb.TagNumber(8)
  $core.bool hasDriver() => $_has(7);
  @$pb.TagNumber(8)
  void clearDriver() => clearField(8);
  @$pb.TagNumber(8)
  Driver ensureDriver() => $_ensure(7);

  @$pb.TagNumber(9)
  Vehicle get vehicle => $_getN(8);
  @$pb.TagNumber(9)
  set vehicle(Vehicle v) { setField(9, v); }
  @$pb.TagNumber(9)
  $core.bool hasVehicle() => $_has(8);
  @$pb.TagNumber(9)
  void clearVehicle() => clearField(9);
  @$pb.TagNumber(9)
  Vehicle ensureVehicle() => $_ensure(8);

  @$pb.TagNumber(10)
  $core.String get invoiceId => $_getSZ(9);
  @$pb.TagNumber(10)
  set invoiceId($core.String v) { $_setString(9, v); }
  @$pb.TagNumber(10)
  $core.bool hasInvoiceId() => $_has(9);
  @$pb.TagNumber(10)
  void clearInvoiceId() => clearField(10);

  @$pb.TagNumber(11)
  Amount get cost => $_getN(10);
  @$pb.TagNumber(11)
  set cost(Amount v) { setField(11, v); }
  @$pb.TagNumber(11)
  $core.bool hasCost() => $_has(10);
  @$pb.TagNumber(11)
  void clearCost() => clearField(11);
  @$pb.TagNumber(11)
  Amount ensureCost() => $_ensure(10);
}

class Driver extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Driver', package: const $pb.PackageName('com.connectroutes.trip'), createEmptyInstance: create)
    ..aOS(1, 'id')
    ..aOS(2, 'pictureUrl')
    ..a<$core.double>(3, 'rating', $pb.PbFieldType.OD)
    ..a<$core.double>(4, 'name', $pb.PbFieldType.OD)
    ..hasRequiredFields = false
  ;

  Driver._() : super();
  factory Driver() => create();
  factory Driver.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Driver.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Driver clone() => Driver()..mergeFromMessage(this);
  Driver copyWith(void Function(Driver) updates) => super.copyWith((message) => updates(message as Driver));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Driver create() => Driver._();
  Driver createEmptyInstance() => create();
  static $pb.PbList<Driver> createRepeated() => $pb.PbList<Driver>();
  @$core.pragma('dart2js:noInline')
  static Driver getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Driver>(create);
  static Driver _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get pictureUrl => $_getSZ(1);
  @$pb.TagNumber(2)
  set pictureUrl($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPictureUrl() => $_has(1);
  @$pb.TagNumber(2)
  void clearPictureUrl() => clearField(2);

  @$pb.TagNumber(3)
  $core.double get rating => $_getN(2);
  @$pb.TagNumber(3)
  set rating($core.double v) { $_setDouble(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasRating() => $_has(2);
  @$pb.TagNumber(3)
  void clearRating() => clearField(3);

  @$pb.TagNumber(4)
  $core.double get name => $_getN(3);
  @$pb.TagNumber(4)
  set name($core.double v) { $_setDouble(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasName() => $_has(3);
  @$pb.TagNumber(4)
  void clearName() => clearField(4);
}

class Vehicle extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Vehicle', package: const $pb.PackageName('com.connectroutes.trip'), createEmptyInstance: create)
    ..aOS(1, 'id')
    ..aOS(2, 'manufacturer')
    ..aOS(3, 'model')
    ..aOS(4, 'plateNo')
    ..a<$core.int>(5, 'color', $pb.PbFieldType.O3)
    ..aOB(6, 'hasAirConditioning')
    ..hasRequiredFields = false
  ;

  Vehicle._() : super();
  factory Vehicle() => create();
  factory Vehicle.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Vehicle.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Vehicle clone() => Vehicle()..mergeFromMessage(this);
  Vehicle copyWith(void Function(Vehicle) updates) => super.copyWith((message) => updates(message as Vehicle));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Vehicle create() => Vehicle._();
  Vehicle createEmptyInstance() => create();
  static $pb.PbList<Vehicle> createRepeated() => $pb.PbList<Vehicle>();
  @$core.pragma('dart2js:noInline')
  static Vehicle getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Vehicle>(create);
  static Vehicle _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get manufacturer => $_getSZ(1);
  @$pb.TagNumber(2)
  set manufacturer($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasManufacturer() => $_has(1);
  @$pb.TagNumber(2)
  void clearManufacturer() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get model => $_getSZ(2);
  @$pb.TagNumber(3)
  set model($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasModel() => $_has(2);
  @$pb.TagNumber(3)
  void clearModel() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get plateNo => $_getSZ(3);
  @$pb.TagNumber(4)
  set plateNo($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasPlateNo() => $_has(3);
  @$pb.TagNumber(4)
  void clearPlateNo() => clearField(4);

  @$pb.TagNumber(5)
  $core.int get color => $_getIZ(4);
  @$pb.TagNumber(5)
  set color($core.int v) { $_setSignedInt32(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasColor() => $_has(4);
  @$pb.TagNumber(5)
  void clearColor() => clearField(5);

  @$pb.TagNumber(6)
  $core.bool get hasAirConditioning => $_getBF(5);
  @$pb.TagNumber(6)
  set hasAirConditioning($core.bool v) { $_setBool(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasHasAirConditioning() => $_has(5);
  @$pb.TagNumber(6)
  void clearHasAirConditioning() => clearField(6);
}

class AutoCompleteAddressRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AutoCompleteAddressRequest', package: const $pb.PackageName('com.connectroutes.trip'), createEmptyInstance: create)
    ..aOS(1, 'description')
    ..hasRequiredFields = false
  ;

  AutoCompleteAddressRequest._() : super();
  factory AutoCompleteAddressRequest() => create();
  factory AutoCompleteAddressRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AutoCompleteAddressRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  AutoCompleteAddressRequest clone() => AutoCompleteAddressRequest()..mergeFromMessage(this);
  AutoCompleteAddressRequest copyWith(void Function(AutoCompleteAddressRequest) updates) => super.copyWith((message) => updates(message as AutoCompleteAddressRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AutoCompleteAddressRequest create() => AutoCompleteAddressRequest._();
  AutoCompleteAddressRequest createEmptyInstance() => create();
  static $pb.PbList<AutoCompleteAddressRequest> createRepeated() => $pb.PbList<AutoCompleteAddressRequest>();
  @$core.pragma('dart2js:noInline')
  static AutoCompleteAddressRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AutoCompleteAddressRequest>(create);
  static AutoCompleteAddressRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get description => $_getSZ(0);
  @$pb.TagNumber(1)
  set description($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasDescription() => $_has(0);
  @$pb.TagNumber(1)
  void clearDescription() => clearField(1);
}

class AutoCompleteAddressResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AutoCompleteAddressResponse', package: const $pb.PackageName('com.connectroutes.trip'), createEmptyInstance: create)
    ..pc<Address>(1, 'addresses', $pb.PbFieldType.PM, subBuilder: Address.create)
    ..hasRequiredFields = false
  ;

  AutoCompleteAddressResponse._() : super();
  factory AutoCompleteAddressResponse() => create();
  factory AutoCompleteAddressResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AutoCompleteAddressResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  AutoCompleteAddressResponse clone() => AutoCompleteAddressResponse()..mergeFromMessage(this);
  AutoCompleteAddressResponse copyWith(void Function(AutoCompleteAddressResponse) updates) => super.copyWith((message) => updates(message as AutoCompleteAddressResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AutoCompleteAddressResponse create() => AutoCompleteAddressResponse._();
  AutoCompleteAddressResponse createEmptyInstance() => create();
  static $pb.PbList<AutoCompleteAddressResponse> createRepeated() => $pb.PbList<AutoCompleteAddressResponse>();
  @$core.pragma('dart2js:noInline')
  static AutoCompleteAddressResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AutoCompleteAddressResponse>(create);
  static AutoCompleteAddressResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Address> get addresses => $_getList(0);
}

class TripHistoryRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('TripHistoryRequest', package: const $pb.PackageName('com.connectroutes.trip'), createEmptyInstance: create)
    ..a<$core.int>(1, 'page', $pb.PbFieldType.O3)
    ..a<$core.int>(2, 'size', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  TripHistoryRequest._() : super();
  factory TripHistoryRequest() => create();
  factory TripHistoryRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TripHistoryRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  TripHistoryRequest clone() => TripHistoryRequest()..mergeFromMessage(this);
  TripHistoryRequest copyWith(void Function(TripHistoryRequest) updates) => super.copyWith((message) => updates(message as TripHistoryRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TripHistoryRequest create() => TripHistoryRequest._();
  TripHistoryRequest createEmptyInstance() => create();
  static $pb.PbList<TripHistoryRequest> createRepeated() => $pb.PbList<TripHistoryRequest>();
  @$core.pragma('dart2js:noInline')
  static TripHistoryRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TripHistoryRequest>(create);
  static TripHistoryRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get page => $_getIZ(0);
  @$pb.TagNumber(1)
  set page($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPage() => $_has(0);
  @$pb.TagNumber(1)
  void clearPage() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get size => $_getIZ(1);
  @$pb.TagNumber(2)
  set size($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasSize() => $_has(1);
  @$pb.TagNumber(2)
  void clearSize() => clearField(2);
}

class TripHistoryResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('TripHistoryResponse', package: const $pb.PackageName('com.connectroutes.trip'), createEmptyInstance: create)
    ..pc<Trip>(1, 'trips', $pb.PbFieldType.PM, subBuilder: Trip.create)
    ..a<$core.int>(2, 'page', $pb.PbFieldType.O3)
    ..a<$core.int>(3, 'size', $pb.PbFieldType.O3)
    ..a<$core.int>(4, 'total', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  TripHistoryResponse._() : super();
  factory TripHistoryResponse() => create();
  factory TripHistoryResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TripHistoryResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  TripHistoryResponse clone() => TripHistoryResponse()..mergeFromMessage(this);
  TripHistoryResponse copyWith(void Function(TripHistoryResponse) updates) => super.copyWith((message) => updates(message as TripHistoryResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TripHistoryResponse create() => TripHistoryResponse._();
  TripHistoryResponse createEmptyInstance() => create();
  static $pb.PbList<TripHistoryResponse> createRepeated() => $pb.PbList<TripHistoryResponse>();
  @$core.pragma('dart2js:noInline')
  static TripHistoryResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TripHistoryResponse>(create);
  static TripHistoryResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Trip> get trips => $_getList(0);

  @$pb.TagNumber(2)
  $core.int get page => $_getIZ(1);
  @$pb.TagNumber(2)
  set page($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPage() => $_has(1);
  @$pb.TagNumber(2)
  void clearPage() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get size => $_getIZ(2);
  @$pb.TagNumber(3)
  set size($core.int v) { $_setSignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasSize() => $_has(2);
  @$pb.TagNumber(3)
  void clearSize() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get total => $_getIZ(3);
  @$pb.TagNumber(4)
  set total($core.int v) { $_setSignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasTotal() => $_has(3);
  @$pb.TagNumber(4)
  void clearTotal() => clearField(4);
}

class SetDefaultAddressRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('SetDefaultAddressRequest', package: const $pb.PackageName('com.connectroutes.trip'), createEmptyInstance: create)
    ..aOS(1, 'placeId')
    ..aOS(2, 'description')
    ..e<AddressType>(3, 'type', $pb.PbFieldType.OE, defaultOrMaker: AddressType.home, valueOf: AddressType.valueOf, enumValues: AddressType.values)
    ..hasRequiredFields = false
  ;

  SetDefaultAddressRequest._() : super();
  factory SetDefaultAddressRequest() => create();
  factory SetDefaultAddressRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SetDefaultAddressRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  SetDefaultAddressRequest clone() => SetDefaultAddressRequest()..mergeFromMessage(this);
  SetDefaultAddressRequest copyWith(void Function(SetDefaultAddressRequest) updates) => super.copyWith((message) => updates(message as SetDefaultAddressRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SetDefaultAddressRequest create() => SetDefaultAddressRequest._();
  SetDefaultAddressRequest createEmptyInstance() => create();
  static $pb.PbList<SetDefaultAddressRequest> createRepeated() => $pb.PbList<SetDefaultAddressRequest>();
  @$core.pragma('dart2js:noInline')
  static SetDefaultAddressRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SetDefaultAddressRequest>(create);
  static SetDefaultAddressRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get placeId => $_getSZ(0);
  @$pb.TagNumber(1)
  set placeId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPlaceId() => $_has(0);
  @$pb.TagNumber(1)
  void clearPlaceId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get description => $_getSZ(1);
  @$pb.TagNumber(2)
  set description($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasDescription() => $_has(1);
  @$pb.TagNumber(2)
  void clearDescription() => clearField(2);

  @$pb.TagNumber(3)
  AddressType get type => $_getN(2);
  @$pb.TagNumber(3)
  set type(AddressType v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasType() => $_has(2);
  @$pb.TagNumber(3)
  void clearType() => clearField(3);
}

class SetDefaultAddressResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('SetDefaultAddressResponse', package: const $pb.PackageName('com.connectroutes.trip'), createEmptyInstance: create)
    ..aOS(1, 'message')
    ..hasRequiredFields = false
  ;

  SetDefaultAddressResponse._() : super();
  factory SetDefaultAddressResponse() => create();
  factory SetDefaultAddressResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SetDefaultAddressResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  SetDefaultAddressResponse clone() => SetDefaultAddressResponse()..mergeFromMessage(this);
  SetDefaultAddressResponse copyWith(void Function(SetDefaultAddressResponse) updates) => super.copyWith((message) => updates(message as SetDefaultAddressResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SetDefaultAddressResponse create() => SetDefaultAddressResponse._();
  SetDefaultAddressResponse createEmptyInstance() => create();
  static $pb.PbList<SetDefaultAddressResponse> createRepeated() => $pb.PbList<SetDefaultAddressResponse>();
  @$core.pragma('dart2js:noInline')
  static SetDefaultAddressResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SetDefaultAddressResponse>(create);
  static SetDefaultAddressResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);
}

