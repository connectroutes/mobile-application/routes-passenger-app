///
//  Generated code. Do not modify.
//  source: trip.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

// ignore_for_file: UNDEFINED_SHOWN_NAME,UNUSED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class TripStatus extends $pb.ProtobufEnum {
  static const TripStatus created = TripStatus._(0, 'created');
  static const TripStatus in_progress = TripStatus._(1, 'in_progress');
  static const TripStatus cancelled = TripStatus._(2, 'cancelled');
  static const TripStatus done = TripStatus._(3, 'done');

  static const $core.List<TripStatus> values = <TripStatus> [
    created,
    in_progress,
    cancelled,
    done,
  ];

  static final $core.Map<$core.int, TripStatus> _byValue = $pb.ProtobufEnum.initByValue(values);
  static TripStatus valueOf($core.int value) => _byValue[value];

  const TripStatus._($core.int v, $core.String n) : super(v, n);
}

class AddressType extends $pb.ProtobufEnum {
  static const AddressType home = AddressType._(0, 'home');
  static const AddressType work = AddressType._(1, 'work');

  static const $core.List<AddressType> values = <AddressType> [
    home,
    work,
  ];

  static final $core.Map<$core.int, AddressType> _byValue = $pb.ProtobufEnum.initByValue(values);
  static AddressType valueOf($core.int value) => _byValue[value];

  const AddressType._($core.int v, $core.String n) : super(v, n);
}

