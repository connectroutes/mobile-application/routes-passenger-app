///
//  Generated code. Do not modify.
//  source: payment.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'payment.pb.dart' as $0;
export 'payment.pb.dart';

class PaymentServiceClient extends $grpc.Client {
  static final _$getPaymentOptions = $grpc.ClientMethod<
          $0.GetPaymentOptionsRequest, $0.GetPaymentOptionsResponse>(
      '/com.connectroutes.payment.PaymentService/GetPaymentOptions',
      ($0.GetPaymentOptionsRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.GetPaymentOptionsResponse.fromBuffer(value));
  static final _$addCard =
      $grpc.ClientMethod<$0.AddCardRequest, $0.AddCardResponse>(
          '/com.connectroutes.payment.PaymentService/AddCard',
          ($0.AddCardRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.AddCardResponse.fromBuffer(value));
  static final _$submitCardAuthorization =
      $grpc.ClientMethod<$0.SubmitCardAuthorizationRequest, $0.AddCardResponse>(
          '/com.connectroutes.payment.PaymentService/SubmitCardAuthorization',
          ($0.SubmitCardAuthorizationRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.AddCardResponse.fromBuffer(value));
  static final _$addCardByReference =
      $grpc.ClientMethod<$0.AddCardByReferenceRequest, $0.AddCardResponse>(
          '/com.connectroutes.payment.PaymentService/AddCardByReference',
          ($0.AddCardByReferenceRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.AddCardResponse.fromBuffer(value));

  PaymentServiceClient($grpc.ClientChannel channel, {$grpc.CallOptions options})
      : super(channel, options: options);

  $grpc.ResponseFuture<$0.GetPaymentOptionsResponse> getPaymentOptions(
      $0.GetPaymentOptionsRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$getPaymentOptions, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.AddCardResponse> addCard($0.AddCardRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$addCard, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.AddCardResponse> submitCardAuthorization(
      $0.SubmitCardAuthorizationRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$submitCardAuthorization, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.AddCardResponse> addCardByReference(
      $0.AddCardByReferenceRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$addCardByReference, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }
}

abstract class PaymentServiceBase extends $grpc.Service {
  $core.String get $name => 'com.connectroutes.payment.PaymentService';

  PaymentServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.GetPaymentOptionsRequest,
            $0.GetPaymentOptionsResponse>(
        'GetPaymentOptions',
        getPaymentOptions_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.GetPaymentOptionsRequest.fromBuffer(value),
        ($0.GetPaymentOptionsResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.AddCardRequest, $0.AddCardResponse>(
        'AddCard',
        addCard_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.AddCardRequest.fromBuffer(value),
        ($0.AddCardResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SubmitCardAuthorizationRequest,
            $0.AddCardResponse>(
        'SubmitCardAuthorization',
        submitCardAuthorization_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.SubmitCardAuthorizationRequest.fromBuffer(value),
        ($0.AddCardResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.AddCardByReferenceRequest, $0.AddCardResponse>(
            'AddCardByReference',
            addCardByReference_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.AddCardByReferenceRequest.fromBuffer(value),
            ($0.AddCardResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.GetPaymentOptionsResponse> getPaymentOptions_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.GetPaymentOptionsRequest> request) async {
    return getPaymentOptions(call, await request);
  }

  $async.Future<$0.AddCardResponse> addCard_Pre(
      $grpc.ServiceCall call, $async.Future<$0.AddCardRequest> request) async {
    return addCard(call, await request);
  }

  $async.Future<$0.AddCardResponse> submitCardAuthorization_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.SubmitCardAuthorizationRequest> request) async {
    return submitCardAuthorization(call, await request);
  }

  $async.Future<$0.AddCardResponse> addCardByReference_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.AddCardByReferenceRequest> request) async {
    return addCardByReference(call, await request);
  }

  $async.Future<$0.GetPaymentOptionsResponse> getPaymentOptions(
      $grpc.ServiceCall call, $0.GetPaymentOptionsRequest request);
  $async.Future<$0.AddCardResponse> addCard(
      $grpc.ServiceCall call, $0.AddCardRequest request);
  $async.Future<$0.AddCardResponse> submitCardAuthorization(
      $grpc.ServiceCall call, $0.SubmitCardAuthorizationRequest request);
  $async.Future<$0.AddCardResponse> addCardByReference(
      $grpc.ServiceCall call, $0.AddCardByReferenceRequest request);
}
