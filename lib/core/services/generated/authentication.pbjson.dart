///
//  Generated code. Do not modify.
//  source: authentication.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const RequestOtpRequest$json = const {
  '1': 'RequestOtpRequest',
  '2': const [
    const {'1': 'phone_number', '3': 1, '4': 1, '5': 9, '10': 'phoneNumber'},
  ],
};

const RequestOtpResponse$json = const {
  '1': 'RequestOtpResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
  ],
};

const SubmitOtpRequest$json = const {
  '1': 'SubmitOtpRequest',
  '2': const [
    const {'1': 'otp', '3': 1, '4': 1, '5': 5, '10': 'otp'},
    const {'1': 'phone_number', '3': 2, '4': 1, '5': 9, '10': 'phoneNumber'},
  ],
};

const SubmitOtpResponse$json = const {
  '1': 'SubmitOtpResponse',
  '2': const [
    const {'1': 'otp_authentication_id', '3': 1, '4': 1, '5': 9, '10': 'otpAuthenticationId'},
    const {'1': 'token', '3': 2, '4': 1, '5': 9, '10': 'token'},
    const {'1': 'user_data', '3': 3, '4': 1, '5': 9, '10': 'userData'},
    const {'1': 'is_new_user', '3': 4, '4': 1, '5': 8, '10': 'isNewUser'},
  ],
};

const SubmitPersonalInfoRequest$json = const {
  '1': 'SubmitPersonalInfoRequest',
  '2': const [
    const {'1': 'full_name', '3': 1, '4': 1, '5': 9, '10': 'fullName'},
    const {'1': 'email_address', '3': 2, '4': 1, '5': 9, '10': 'emailAddress'},
    const {'1': 'password', '3': 3, '4': 1, '5': 9, '10': 'password'},
    const {'1': 'phone_number', '3': 4, '4': 1, '5': 9, '10': 'phoneNumber'},
    const {'1': 'otp_authentication_id', '3': 5, '4': 1, '5': 9, '10': 'otpAuthenticationId'},
  ],
};

const SubmitPersonalInfoResponse$json = const {
  '1': 'SubmitPersonalInfoResponse',
  '2': const [
    const {'1': 'token', '3': 1, '4': 1, '5': 9, '10': 'token'},
    const {'1': 'user_data', '3': 2, '4': 1, '5': 9, '10': 'userData'},
  ],
};

