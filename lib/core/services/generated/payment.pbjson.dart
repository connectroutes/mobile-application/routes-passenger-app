///
//  Generated code. Do not modify.
//  source: payment.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const PaymentMethod$json = const {
  '1': 'PaymentMethod',
  '2': const [
    const {'1': 'cash', '2': 0},
    const {'1': 'card', '2': 1},
  ],
};

const CardType$json = const {
  '1': 'CardType',
  '2': const [
    const {'1': 'visa', '2': 0},
    const {'1': 'mastercard', '2': 1},
  ],
};

const PaymentOption$json = const {
  '1': 'PaymentOption',
  '2': const [
    const {'1': 'method', '3': 1, '4': 1, '5': 14, '6': '.com.connectroutes.payment.PaymentMethod', '10': 'method'},
    const {'1': 'id', '3': 2, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'cardType', '3': 3, '4': 1, '5': 14, '6': '.com.connectroutes.payment.CardType', '10': 'cardType'},
    const {'1': 'cardLast4', '3': 4, '4': 1, '5': 9, '10': 'cardLast4'},
    const {'1': 'bin', '3': 5, '4': 1, '5': 9, '10': 'bin'},
    const {'1': 'expiryMonth', '3': 6, '4': 1, '5': 9, '10': 'expiryMonth'},
    const {'1': 'expiryYear', '3': 7, '4': 1, '5': 9, '10': 'expiryYear'},
    const {'1': 'lastUsed', '3': 8, '4': 1, '5': 3, '10': 'lastUsed'},
  ],
};

const GetPaymentOptionsRequest$json = const {
  '1': 'GetPaymentOptionsRequest',
};

const GetPaymentOptionsResponse$json = const {
  '1': 'GetPaymentOptionsResponse',
  '2': const [
    const {'1': 'options', '3': 1, '4': 3, '5': 11, '6': '.com.connectroutes.payment.PaymentOption', '10': 'options'},
  ],
};

const AddCardRequest$json = const {
  '1': 'AddCardRequest',
  '2': const [
    const {'1': 'cardNo', '3': 1, '4': 1, '5': 9, '10': 'cardNo'},
    const {'1': 'cardExpiryMonth', '3': 2, '4': 1, '5': 9, '10': 'cardExpiryMonth'},
    const {'1': 'cardExpiryYear', '3': 3, '4': 1, '5': 9, '10': 'cardExpiryYear'},
    const {'1': 'cardCvv', '3': 4, '4': 1, '5': 9, '10': 'cardCvv'},
    const {'1': 'cardPin', '3': 5, '4': 1, '5': 9, '10': 'cardPin'},
  ],
};

const AddCardResponse$json = const {
  '1': 'AddCardResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'action', '3': 2, '4': 1, '5': 9, '10': 'action'},
    const {'1': 'action_url', '3': 3, '4': 1, '5': 9, '10': 'actionUrl'},
    const {'1': 'reference', '3': 4, '4': 1, '5': 9, '10': 'reference'},
    const {'1': 'card', '3': 5, '4': 1, '5': 11, '6': '.com.connectroutes.payment.PaymentOption', '10': 'card'},
  ],
};

const SubmitCardAuthorizationRequest$json = const {
  '1': 'SubmitCardAuthorizationRequest',
  '2': const [
    const {'1': 'action', '3': 1, '4': 1, '5': 9, '10': 'action'},
    const {'1': 'value', '3': 2, '4': 1, '5': 9, '10': 'value'},
    const {'1': 'reference', '3': 3, '4': 1, '5': 9, '10': 'reference'},
  ],
};

const AddCardByReferenceRequest$json = const {
  '1': 'AddCardByReferenceRequest',
  '2': const [
    const {'1': 'reference', '3': 1, '4': 1, '5': 9, '10': 'reference'},
  ],
};

