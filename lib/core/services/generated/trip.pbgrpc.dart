///
//  Generated code. Do not modify.
//  source: trip.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'trip.pb.dart' as $0;
export 'trip.pb.dart';

class TripServiceClient extends $grpc.Client {
  static final _$autoCompleteAddress = $grpc.ClientMethod<
          $0.AutoCompleteAddressRequest, $0.AutoCompleteAddressResponse>(
      '/com.connectroutes.trip.TripService/AutoCompleteAddress',
      ($0.AutoCompleteAddressRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.AutoCompleteAddressResponse.fromBuffer(value));
  static final _$tripHistory =
      $grpc.ClientMethod<$0.TripHistoryRequest, $0.TripHistoryResponse>(
          '/com.connectroutes.trip.TripService/TripHistory',
          ($0.TripHistoryRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.TripHistoryResponse.fromBuffer(value));
  static final _$setDefaultAddress = $grpc.ClientMethod<
          $0.SetDefaultAddressRequest, $0.SetDefaultAddressResponse>(
      '/com.connectroutes.trip.TripService/SetDefaultAddress',
      ($0.SetDefaultAddressRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.SetDefaultAddressResponse.fromBuffer(value));

  TripServiceClient($grpc.ClientChannel channel, {$grpc.CallOptions options})
      : super(channel, options: options);

  $grpc.ResponseFuture<$0.AutoCompleteAddressResponse> autoCompleteAddress(
      $0.AutoCompleteAddressRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$autoCompleteAddress, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.TripHistoryResponse> tripHistory(
      $0.TripHistoryRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$tripHistory, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.SetDefaultAddressResponse> setDefaultAddress(
      $0.SetDefaultAddressRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$setDefaultAddress, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }
}

abstract class TripServiceBase extends $grpc.Service {
  $core.String get $name => 'com.connectroutes.trip.TripService';

  TripServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.AutoCompleteAddressRequest,
            $0.AutoCompleteAddressResponse>(
        'AutoCompleteAddress',
        autoCompleteAddress_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.AutoCompleteAddressRequest.fromBuffer(value),
        ($0.AutoCompleteAddressResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.TripHistoryRequest, $0.TripHistoryResponse>(
            'TripHistory',
            tripHistory_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.TripHistoryRequest.fromBuffer(value),
            ($0.TripHistoryResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SetDefaultAddressRequest,
            $0.SetDefaultAddressResponse>(
        'SetDefaultAddress',
        setDefaultAddress_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.SetDefaultAddressRequest.fromBuffer(value),
        ($0.SetDefaultAddressResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.AutoCompleteAddressResponse> autoCompleteAddress_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.AutoCompleteAddressRequest> request) async {
    return autoCompleteAddress(call, await request);
  }

  $async.Future<$0.TripHistoryResponse> tripHistory_Pre($grpc.ServiceCall call,
      $async.Future<$0.TripHistoryRequest> request) async {
    return tripHistory(call, await request);
  }

  $async.Future<$0.SetDefaultAddressResponse> setDefaultAddress_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.SetDefaultAddressRequest> request) async {
    return setDefaultAddress(call, await request);
  }

  $async.Future<$0.AutoCompleteAddressResponse> autoCompleteAddress(
      $grpc.ServiceCall call, $0.AutoCompleteAddressRequest request);
  $async.Future<$0.TripHistoryResponse> tripHistory(
      $grpc.ServiceCall call, $0.TripHistoryRequest request);
  $async.Future<$0.SetDefaultAddressResponse> setDefaultAddress(
      $grpc.ServiceCall call, $0.SetDefaultAddressRequest request);
}
