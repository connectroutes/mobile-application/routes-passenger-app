///
//  Generated code. Do not modify.
//  source: trip.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const TripStatus$json = const {
  '1': 'TripStatus',
  '2': const [
    const {'1': 'created', '2': 0},
    const {'1': 'in_progress', '2': 1},
    const {'1': 'cancelled', '2': 2},
    const {'1': 'done', '2': 3},
  ],
};

const AddressType$json = const {
  '1': 'AddressType',
  '2': const [
    const {'1': 'home', '2': 0},
    const {'1': 'work', '2': 1},
  ],
};

const Address$json = const {
  '1': 'Address',
  '2': const [
    const {'1': 'description', '3': 1, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'place_id', '3': 2, '4': 1, '5': 9, '10': 'placeId'},
  ],
};

const Amount$json = const {
  '1': 'Amount',
  '2': const [
    const {'1': 'currency', '3': 1, '4': 1, '5': 9, '10': 'currency'},
    const {'1': 'amount', '3': 2, '4': 1, '5': 1, '10': 'amount'},
  ],
};

const Trip$json = const {
  '1': 'Trip',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'pickup_address', '3': 2, '4': 1, '5': 11, '6': '.com.connectroutes.trip.Address', '10': 'pickupAddress'},
    const {'1': 'destination_address', '3': 3, '4': 1, '5': 11, '6': '.com.connectroutes.trip.Address', '10': 'destinationAddress'},
    const {'1': 'status', '3': 4, '4': 1, '5': 14, '6': '.com.connectroutes.trip.TripStatus', '10': 'status'},
    const {'1': 'request_time', '3': 5, '4': 1, '5': 3, '10': 'requestTime'},
    const {'1': 'pickup_time', '3': 6, '4': 1, '5': 3, '10': 'pickupTime'},
    const {'1': 'end_time', '3': 7, '4': 1, '5': 3, '10': 'endTime'},
    const {'1': 'driver', '3': 8, '4': 1, '5': 11, '6': '.com.connectroutes.trip.Driver', '10': 'driver'},
    const {'1': 'vehicle', '3': 9, '4': 1, '5': 11, '6': '.com.connectroutes.trip.Vehicle', '10': 'vehicle'},
    const {'1': 'invoice_id', '3': 10, '4': 1, '5': 9, '10': 'invoiceId'},
    const {'1': 'cost', '3': 11, '4': 1, '5': 11, '6': '.com.connectroutes.trip.Amount', '10': 'cost'},
  ],
};

const Driver$json = const {
  '1': 'Driver',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'picture_url', '3': 2, '4': 1, '5': 9, '10': 'pictureUrl'},
    const {'1': 'rating', '3': 3, '4': 1, '5': 1, '10': 'rating'},
    const {'1': 'name', '3': 4, '4': 1, '5': 1, '10': 'name'},
  ],
};

const Vehicle$json = const {
  '1': 'Vehicle',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'manufacturer', '3': 2, '4': 1, '5': 9, '10': 'manufacturer'},
    const {'1': 'model', '3': 3, '4': 1, '5': 9, '10': 'model'},
    const {'1': 'plate_no', '3': 4, '4': 1, '5': 9, '10': 'plateNo'},
    const {'1': 'color', '3': 5, '4': 1, '5': 5, '10': 'color'},
    const {'1': 'has_air_conditioning', '3': 6, '4': 1, '5': 8, '10': 'hasAirConditioning'},
  ],
};

const AutoCompleteAddressRequest$json = const {
  '1': 'AutoCompleteAddressRequest',
  '2': const [
    const {'1': 'description', '3': 1, '4': 1, '5': 9, '10': 'description'},
  ],
};

const AutoCompleteAddressResponse$json = const {
  '1': 'AutoCompleteAddressResponse',
  '2': const [
    const {'1': 'addresses', '3': 1, '4': 3, '5': 11, '6': '.com.connectroutes.trip.Address', '10': 'addresses'},
  ],
};

const TripHistoryRequest$json = const {
  '1': 'TripHistoryRequest',
  '2': const [
    const {'1': 'page', '3': 1, '4': 1, '5': 5, '10': 'page'},
    const {'1': 'size', '3': 2, '4': 1, '5': 5, '10': 'size'},
  ],
};

const TripHistoryResponse$json = const {
  '1': 'TripHistoryResponse',
  '2': const [
    const {'1': 'trips', '3': 1, '4': 3, '5': 11, '6': '.com.connectroutes.trip.Trip', '10': 'trips'},
    const {'1': 'page', '3': 2, '4': 1, '5': 5, '10': 'page'},
    const {'1': 'size', '3': 3, '4': 1, '5': 5, '10': 'size'},
    const {'1': 'total', '3': 4, '4': 1, '5': 5, '10': 'total'},
  ],
};

const SetDefaultAddressRequest$json = const {
  '1': 'SetDefaultAddressRequest',
  '2': const [
    const {'1': 'place_id', '3': 1, '4': 1, '5': 9, '10': 'placeId'},
    const {'1': 'description', '3': 2, '4': 1, '5': 9, '10': 'description'},
    const {'1': 'type', '3': 3, '4': 1, '5': 14, '6': '.com.connectroutes.trip.AddressType', '10': 'type'},
  ],
};

const SetDefaultAddressResponse$json = const {
  '1': 'SetDefaultAddressResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
  ],
};

