import 'package:grpc/grpc.dart';
import 'package:routes_passenger_app/core/services/api.dart';
import 'package:routes_passenger_app/core/services/generated/authentication.pbgrpc.dart';
import 'package:routes_passenger_app/locator.dart';

class PromoService{

  Api api = locator<Api>();
 Future<RequestOtpResponse> requestOtp(String phoneNumber) async{
    print("request otp");
    RequestOtpRequest request = RequestOtpRequest();
    request.phoneNumber = phoneNumber;

    var client = AuthenticationServiceClient(api.clientChannel);
    return await client.requestOtp(request, options: api.getCallOptions());
  }


}
