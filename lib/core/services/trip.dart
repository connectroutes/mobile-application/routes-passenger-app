import 'package:routes_passenger_app/core/services/api.dart';
import 'package:routes_passenger_app/core/services/generated/trip.pbgrpc.dart';
import 'package:routes_passenger_app/locator.dart';

class TripService{

  Api api = locator<Api>();
 Future<AutoCompleteAddressResponse> autoCompleteAddress(String description) async{
    AutoCompleteAddressRequest request = AutoCompleteAddressRequest();
    request.description = description;
    var client = TripServiceClient(api.clientChannel);
    return await client.autoCompleteAddress(request, options: api.getCallOptions());
  }


  Future<SetDefaultAddressResponse> setDefaultAddress(String description, String placeId, AddressType type) async{
    SetDefaultAddressRequest request = SetDefaultAddressRequest();
    request.description = description;
    request.placeId = placeId;
    request.type = type;
    var client = TripServiceClient(api.clientChannel);
    return await client.setDefaultAddress(request, options: api.getCallOptions());
  }


}
