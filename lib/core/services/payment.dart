import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/services/api.dart';
import 'package:routes_passenger_app/core/services/generated/payment.pbgrpc.dart';
import 'package:routes_passenger_app/locator.dart';
import 'dart:convert';

class PaymentService{

  Api api = locator<Api>();
  List<PaymentOption> paymentOptions = [];
  String selectedPaymentOption = "cash";

  Future<GetPaymentOptionsResponse> getPaymentOptions() async{
    GetPaymentOptionsRequest request = GetPaymentOptionsRequest();
    var client = PaymentServiceClient(api.clientChannel);
    return await client.getPaymentOptions(request, options: api.getCallOptions());
  }

  Future<AddCardResponse> addCardByReference(String reference)async{
    AddCardByReferenceRequest request = AddCardByReferenceRequest()
    ..reference = reference;

    var client = PaymentServiceClient(api.clientChannel);
    return await client.addCardByReference(request, options: api.getCallOptions());
  }

  Future<AddCardResponse> addCard(String number, String cvv, String expiryMonth, String expiryYear, String pin) async{
    AddCardRequest request = AddCardRequest();

    request.cardCvv = cvv;
    request.cardNo = number;
    request.cardExpiryMonth = expiryMonth;
    request.cardExpiryYear = expiryYear;
    request.cardPin = pin;

    var client = PaymentServiceClient(api.clientChannel);

    return await client.addCard(request, options: api.getCallOptions());
  }

  Future<AddCardResponse> submitCardAuthorization(String action, String value, String reference ) async{
    SubmitCardAuthorizationRequest request = SubmitCardAuthorizationRequest();
    request.action = action;
    request.value = value;
    request.reference = reference;

    var client = PaymentServiceClient(api.clientChannel);

    return await client.submitCardAuthorization(request, options: api.getCallOptions());
  }


  setPaymentOption(String id){
    Utilities.saveString("selected_payment_option", id);
    selectedPaymentOption = id;
  }
  _loadSelectedPaymentOption()async {
    String selectedOption = await Utilities.getString("selected_payment_option");
    if (selectedOption != null){
      selectedPaymentOption = selectedOption;
    } else {

      if (paymentOptions.length == 2){
        // only one card is active
        selectedPaymentOption = paymentOptions[0].id; // pick the card
        return;
      }

      PaymentOption lastUsedCard = paymentOptions[0];

      for (var option in paymentOptions){
        if (option.lastUsed > lastUsedCard.lastUsed){
          lastUsedCard = option;
        }
      }

      selectedPaymentOption = lastUsedCard.id;

    }
  }

  loadPaymentOptions()async {
    var storedOptionsString = await Utilities.getString("payment_options");
    if (storedOptionsString!= null){
       var storedOptions = json.decode(storedOptionsString);
       for  (var o in storedOptions){
         paymentOptions.add(PaymentOption.fromJson(json.encode(o)));
       }
    }
    _loadSelectedPaymentOption();
   fetchPaymentOptions();
  }



  fetchPaymentOptions()async{
   var getPaymentOptionsResponse = await getPaymentOptions();
   List<String> options = [];


   for (var option in getPaymentOptionsResponse.options){
     options.add(option.writeToJson());
   }
   Utilities.saveString("payment_options", options.toString());
   paymentOptions = getPaymentOptionsResponse.options;

  }





}
