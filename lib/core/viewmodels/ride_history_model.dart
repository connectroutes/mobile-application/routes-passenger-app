import 'package:flutter/cupertino.dart';
import 'package:routes_passenger_app/core/services/generated/trip.pbgrpc.dart';
import 'package:routes_passenger_app/locator.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/enums/viewstate.dart';
import 'package:routes_passenger_app/core/viewmodels/base_model.dart';


class RideHistoryModel extends BaseModel {
  var _userService = locator<UserService>();

  String editingField;
  String editingFieldDescription;

  List<Trip> trips = [Trip(), Trip()];

  User user;
  RideHistoryModel(){
    user = _userService.getCurrentUser();
  }


}