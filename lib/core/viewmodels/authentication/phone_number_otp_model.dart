import 'package:flutter/material.dart';
import 'package:routes_passenger_app/core/enums/viewstate.dart';
import 'package:routes_passenger_app/core/services/authentication.dart';
import 'package:routes_passenger_app/core/services/generated/authentication.pb.dart';
import 'package:routes_passenger_app/core/viewmodels/base_model.dart';

import '../../../locator.dart';

class PhoneNumberOtpModel extends BaseModel {
  var authenticationService = locator<AuthenticationService>();

  bool inputValid = false;

  bool resendCodeEnabled = false;

  ValueChanged<String> inputChanged;

  String otp;


  PhoneNumberOtpModel(){
    inputChanged = (String value){
      print("otp set"+ value);
      this.otp = value;
      inputValid = true;
      notifyListeners();
    };
  }


  Future<SubmitOtpResponse> submitOtp(String phoneNumber)async{
    setState(ViewState.Busy);
    var response = await authenticationService.submitOtp(phoneNumber, int.parse(otp));
    return response;
  }

  resendOtp(String phoneNumber)async{
    resendCodeEnabled = false;
    notifyListeners();
    var response = await authenticationService.requestOtp(phoneNumber);
    if (response.message.isNotEmpty){
      print("otp resent");
    }

  }

  enableResetCountDown(){
    resendCodeEnabled = true;
    notifyListeners();
  }


}