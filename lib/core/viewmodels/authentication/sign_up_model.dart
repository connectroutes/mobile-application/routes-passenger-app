import 'package:flutter/material.dart';
import 'package:routes_passenger_app/core/enums/viewstate.dart';
import 'package:routes_passenger_app/core/services/authentication.dart';
import 'package:routes_passenger_app/core/services/generated/authentication.pb.dart';
import 'package:routes_passenger_app/core/viewmodels/base_model.dart';
import 'package:routes_passenger_app/locator.dart';
import 'package:routes_app_lib/routes_app_lib.dart';


import '../../../locator.dart';

class SignUpModel extends BaseModel {
  var authenticationService = locator<AuthenticationService>();

  TextEditingController nameController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();


  bool inputValid = false;
  String fullName, emailAddress, password;

  UserService userService = locator<UserService>();

  SignUpModel(){
    nameController.addListener((){
      fullName = nameController.text;
      _validateInput();
    });

    emailController.addListener((){
      emailAddress = emailController.text;
      _validateInput();
    });

    passwordController.addListener((){
      password = passwordController.text;
      _validateInput();
    });
  }

  _validateInput(){
    if (fullName.contains(" ") && fullName.length > 2 && RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(emailAddress) && password.length >= 6){
      if (!inputValid){
        inputValid = true;
        notifyListeners();
      }
    } else {
      if (inputValid){
        inputValid = false;
        notifyListeners();
      }
    }
  }

  Future<SubmitPersonalInfoResponse> signUp(String phoneNumber, String otpAuthenticationId)async{

    try{
      var response = await authenticationService.submitPersonalInfo(phoneNumber, otpAuthenticationId, fullName, emailAddress, password);
      userService.storeUserToken(response.token, response.userData);
      return response;

    } catch(e){
      print(e);
      return null;
    }

  }

}