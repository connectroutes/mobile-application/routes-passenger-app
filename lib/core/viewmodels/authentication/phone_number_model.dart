import 'package:flutter/material.dart';
import 'package:routes_passenger_app/core/enums/viewstate.dart';
import 'package:routes_passenger_app/core/services/authentication.dart';
import 'package:routes_passenger_app/core/viewmodels/base_model.dart';

import '../../../locator.dart';

class PhoneNumberModel extends BaseModel {
  var authenticationService = locator<AuthenticationService>();

  bool inputValid = false;
  TextEditingController phoneNumberController = TextEditingController();

  PhoneNumberModel(){
    phoneNumberController.addListener((){
      /*if (phoneNumberController.text.length == 3 || phoneNumberController.text.length == 7){
        phoneNumberController.text = phoneNumberController.text + " ";
        phoneNumberController.selection = TextSelection.fromPosition(TextPosition(offset: phoneNumberController.text.length));
      }*/

      if (phoneNumberController.text.length >= 10){
        inputValid = true;
        notifyListeners();
      } else if(inputValid) {
        inputValid = false;
        notifyListeners();
      }
    });
  }

  Future<bool> requestOTP(String phoneNumber) async {
    setState(ViewState.Busy);
    print('requesting otp');
    try{
      var response = await authenticationService.requestOtp(phoneNumber);

      if (response.message.isNotEmpty){
        print("sent");
      }
      print(response);

      setState(ViewState.Idle);
      return true;
    } catch(e){
      print("error");
      print(e);
      return false;
    }

  }

  @override
  void dispose() {
    super.dispose();
    phoneNumberController.dispose();
  }


}