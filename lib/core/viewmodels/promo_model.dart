
import 'package:flutter/cupertino.dart';
import 'package:routes_passenger_app/core/services/promo.dart';
import 'package:routes_passenger_app/locator.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/viewmodels/base_model.dart';


class PromoModel extends BaseModel {
  var _userService = locator<UserService>();
  var _promoService = locator<PromoService>();

  bool inputValid = false;

  bool submittingPromoCode = false;

  TextEditingController promoCodeController = TextEditingController();

  User user = new User(null);
  String helloText = "";
  PromoModel(){
    user = _userService.getCurrentUser();

    promoCodeController.addListener((){
      if (promoCodeController.text.length >= 1){
        inputValid = true;
        notifyListeners();
      } else if(inputValid) {
        inputValid = false;
        notifyListeners();
      }
    });

  }

  submitPromoCode(){
    submittingPromoCode = true;
    notifyListeners();
  }



}