
import 'package:flutter/cupertino.dart';
import 'package:routes_passenger_app/core/services/promo.dart';
import 'package:routes_passenger_app/locator.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/viewmodels/base_model.dart';


class ReferModel extends BaseModel {
  var _userService = locator<UserService>();
  var _promoService = locator<PromoService>();


  TextEditingController referrerCodeController;

  User user = new User(null);
  String helloText = "";
  ReferModel(){
    user = _userService.getCurrentUser();
    referrerCodeController = TextEditingController(text: user.getReferrerCode());
  }


}