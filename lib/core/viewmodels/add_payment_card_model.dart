
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:routes_passenger_app/core/services/generated/trip.pbgrpc.dart';
import 'package:routes_passenger_app/core/services/payment.dart';
import 'package:routes_passenger_app/core/services/promo.dart';
import 'package:routes_passenger_app/core/services/trip.dart';
import 'package:routes_passenger_app/locator.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/viewmodels/base_model.dart';


class AddPaymentCardModel extends BaseModel {
  var _paymentService = locator<PaymentService>();

  String cardNumber= "" , expiryDate = "", cardHolderName = "", cvv = "", bankName = "", pin = "", otp = "";

  String action = "";
  String actionUrl = "";
  String actionMessage = "";
  String actionValue= "";
  String paymentReference = "";
  String error = "";

  bool loading = false;

  bool showBackSide = false;

  FocusNode cvvFocusNode = FocusNode();

  AddPaymentCardModel(){
    cvvFocusNode.addListener(() {
      cvvFocusNode.hasFocus ? showBackSide = true : showBackSide = false;
      notifyListeners();
    });
  }

  //complete async payment
  addCardFromReference()async{
    try{
      var response = await _paymentService.addCardByReference(paymentReference);
      loading = false;
      action = response.action;
      if (action == "success"){
        _paymentService.paymentOptions.insert(0, response.card); //add card to existing cards
        _paymentService.fetchPaymentOptions();
      }
      notifyListeners();
    } catch (e){
      print(e);
      loading = false;
      error = e.message;
      notifyListeners();
    }
  }

  addCard()async{
    error = ""; // reset error
    loading = true;
    notifyListeners();
    var expiryMonth = expiryDate.split("/")[0];
    var expiryYear = expiryDate.split("/")[1];

    try{
      var response = await _paymentService.addCard(cardNumber, cvv, expiryMonth, expiryYear, pin);
      loading = false;
      action = response.action;
      if (action == "success"){
        _paymentService.paymentOptions.add(response.card); //add card to existing cards
        _paymentService.fetchPaymentOptions();
      }
      actionMessage = response.message;
      paymentReference = response.reference;
      actionUrl = response.actionUrl;
      notifyListeners();
    } catch (e){
      print(e);
      loading = false;
      error = e.message;
      notifyListeners();
    }

  }

  submitOtp()async{
    error = "";
    loading = true;
    notifyListeners();
    try{
      var response = await _paymentService.submitCardAuthorization(action, actionValue, paymentReference);
      loading = false;
      action = response.action;
      if (action == "success"){
        _paymentService.paymentOptions.add(response.card); //add card to existing cards
        _paymentService.fetchPaymentOptions();
      }
      actionMessage = response.message;
      paymentReference = response.reference;
      actionUrl = response.actionUrl;
      notifyListeners();
    } catch (e){
      print(e);
      loading = false;
      error = e.message;
      notifyListeners();
    }

  }

  @override
  void dispose() {
    super.dispose();
    cvvFocusNode.dispose();
  }


}