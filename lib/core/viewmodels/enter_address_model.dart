
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:routes_passenger_app/core/services/generated/trip.pbgrpc.dart';
import 'package:routes_passenger_app/core/services/promo.dart';
import 'package:routes_passenger_app/core/services/trip.dart';
import 'package:routes_passenger_app/locator.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/viewmodels/base_model.dart';


class EnterAddressModel extends BaseModel {
  var _userService = locator<UserService>();
  var _tripService = locator<TripService>();

  TextEditingController pickupController = TextEditingController();
  TextEditingController destinationController = TextEditingController();

  List<Address> suggestions = [];

  User user;

  int _lastSentInputTime = 0;

  FocusNode pickupFocusNode = FocusNode();
  FocusNode destinationFocusNode = FocusNode();

  EnterAddressModel(){
    user = _userService.getCurrentUser();
    var listener = (){
      TimeOfDay.now();
      int currentTime = DateTime.now().millisecondsSinceEpoch;

      String description;

      if (pickupFocusNode.hasFocus){
        description = pickupController.text;
      } else {
        description = destinationController.text;
      }


      if (description.isNotEmpty){
        if (_lastSentInputTime == 0 || currentTime - _lastSentInputTime >= 500){
          _lastSentInputTime = currentTime;
          getAutoCompleteSuggestions(description);
        }
      }

    };
    pickupController.addListener(listener);
    destinationController.addListener(listener);

  }

  getAutoCompleteSuggestions(String description)async{
    print("get autocomplete");
    int then = DateTime.now().millisecondsSinceEpoch;
    var response = await _tripService.autoCompleteAddress(description);
    suggestions = response.addresses;
    print(DateTime.now().millisecondsSinceEpoch - then);
    print(suggestions);
    notifyListeners();
  }

  @override
  void dispose() {
    super.dispose();
    pickupFocusNode.dispose();
    destinationFocusNode.dispose();
  }


}