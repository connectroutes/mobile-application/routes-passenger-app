
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:routes_passenger_app/core/services/generated/trip.pbgrpc.dart';
import 'package:routes_passenger_app/core/services/promo.dart';
import 'package:routes_passenger_app/core/services/trip.dart';
import 'package:routes_passenger_app/locator.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/viewmodels/base_model.dart';


class AddHomeWorkModel extends BaseModel {
  var _userService = locator<UserService>();
  var _tripService = locator<TripService>();

  TextEditingController addressController = TextEditingController();

  List<Address> suggestions = [];

  User user;

  int _lastSentInputTime = 0;

  AddHomeWorkModel(){
    user = _userService.getCurrentUser();
    var listener = (){
      TimeOfDay.now();
      int currentTime = DateTime.now().millisecondsSinceEpoch;
      String description = addressController.text;

      if (description.isNotEmpty){
        if (_lastSentInputTime == 0 || currentTime - _lastSentInputTime >= 500){
          _lastSentInputTime = currentTime;
          getSuggestions(description);
        }
      }

    };
    addressController.addListener(listener);

  }

  getSuggestions(String description)async{
    print("get autocomplete");
    int then = DateTime.now().millisecondsSinceEpoch;
    var response = await _tripService.autoCompleteAddress(description);
    suggestions = response.addresses;
    print(DateTime.now().millisecondsSinceEpoch - then);
    print(suggestions);
    notifyListeners();
  }

  setDefaultAddress(String description, String placeId, AddressType type)async{
    print("set defualt address");
    var response = await _tripService.setDefaultAddress(description, placeId, type);
    print(response);
  }



}