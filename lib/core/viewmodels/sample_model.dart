
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:routes_passenger_app/core/services/generated/trip.pbgrpc.dart';
import 'package:routes_passenger_app/core/services/promo.dart';
import 'package:routes_passenger_app/core/services/trip.dart';
import 'package:routes_passenger_app/locator.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/viewmodels/base_model.dart';


class SampleModel extends BaseModel {
  var _userService = locator<UserService>();
  var _tripService = locator<TripService>();

  User user;

  int _lastSentInputTime = 0;

  SampleModel(){
    user = _userService.getCurrentUser();

  }

  getSuggestions(String description)async{
    print("get autocomplete");
    int then = DateTime.now().millisecondsSinceEpoch;
    var response = await _tripService.autoCompleteAddress(description);
    notifyListeners();
  }

  setDefaultAddress(String description, String placeId)async{
    print("set defualt address");
    var response = await _tripService.setDefaultAddress(description, placeId, null);
    print(response);
  }



}