import 'package:flutter/cupertino.dart';
import 'package:routes_passenger_app/locator.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/enums/viewstate.dart';
import 'package:routes_passenger_app/core/viewmodels/base_model.dart';


class ProfileModel extends BaseModel {
  var _userService = locator<UserService>();

  TextEditingController fullNameController;
  TextEditingController mobileNumberController;
  TextEditingController emailController;

  String editingField;
  String editingFieldDescription;

  User user;
  ProfileModel(){
    user = _userService.getCurrentUser();
    fullNameController = TextEditingController(text: user.getName());
    emailController = TextEditingController(text: user.getEmailAddress());
    mobileNumberController = TextEditingController(text: user.getPhoneNumber());
  }

  editField(String name){
    switch (name){
      case "name":
        editingFieldDescription = "Edit your full name";
      break;
      case "email":
        editingFieldDescription = "Edit your email";
        break;
      case "phone":
        editingFieldDescription = "Update your mobile number and we’ll send a verification code to this number";
        break;
    }
    editingField = name;
    notifyListeners();
  }
  logOut(){
    _userService.deleteUserToken();
  }


}