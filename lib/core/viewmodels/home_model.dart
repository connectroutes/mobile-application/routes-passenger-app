
import 'package:flutter/cupertino.dart';
import 'package:routes_passenger_app/core/services/payment.dart';
import 'package:routes_passenger_app/locator.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/enums/viewstate.dart';
import 'package:routes_passenger_app/core/viewmodels/base_model.dart';


class HomeModel extends BaseModel {

  var _userService = locator<UserService>();
  var _locationService = locator<LocationService>();
  var _paymentService = locator<PaymentService>();

  var currentTime = DateTime.now();

  User user = new User(null);
  String helloText = "";
  String pickupAddress = "";
  String destinationAddress = "";
  HomeModel(){
    user = _userService.getCurrentUser();
    helloText = _getHelloText();
    print("call pay");
    _paymentService.loadPaymentOptions();
  }

  _getHelloText(){
    if (currentTime.hour <= 11){
      return "Good morning, ${user.getFirstName()}!";
    }
    if (currentTime.hour <=17){
      return "Good afternoon, ${user.getFirstName()}!";
    }

    return "Good evening, ${user.getFirstName()}!";

  }

  Future<Location> getCurrentLocation()async{
    return _locationService.getCurrentLocation();
  }
}