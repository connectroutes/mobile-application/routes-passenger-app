
import 'package:routes_passenger_app/core/services/generated/payment.pbgrpc.dart';
import 'package:routes_passenger_app/core/services/payment.dart';
import 'package:routes_passenger_app/locator.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/viewmodels/base_model.dart';


class SelectPaymentMethodModel extends BaseModel {
  var _userService = locator<UserService>();
  var _paymentService = locator<PaymentService>();



  User user;

  SelectPaymentMethodModel(){
    user = _userService.getCurrentUser();

  }

  List<PaymentOption> getOptions(){
    return _paymentService.paymentOptions;
  }

  String getSelectedPaymentOption(){
    return _paymentService.selectedPaymentOption;
  }

  setPaymentOption(String id){
    _paymentService.setPaymentOption(id);
    notifyListeners();
  }


  setDefaultAddress(String description, String placeId)async{

  }



}