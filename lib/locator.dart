import 'package:get_it/get_it.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/services/authentication.dart';
import 'package:routes_passenger_app/core/services/payment.dart';
import 'package:routes_passenger_app/core/services/promo.dart';
import 'package:routes_passenger_app/core/services/trip.dart';
import 'package:routes_passenger_app/core/viewmodels/add_home_work_model.dart';
import 'package:routes_passenger_app/core/viewmodels/add_payment_card_model.dart';
import 'package:routes_passenger_app/core/viewmodels/authentication/sign_up_model.dart';
import 'package:routes_passenger_app/core/viewmodels/enter_address_model.dart';
import 'package:routes_passenger_app/core/viewmodels/home_model.dart';
import 'package:routes_passenger_app/core/viewmodels/authentication/phone_number_model.dart';
import 'package:routes_passenger_app/core/viewmodels/authentication/phone_number_otp_model.dart';
import 'package:routes_passenger_app/core/viewmodels/profile_model.dart';
import 'package:routes_passenger_app/core/viewmodels/promo_model.dart';
import 'package:routes_passenger_app/core/viewmodels/refer_model.dart';
import 'package:routes_passenger_app/core/viewmodels/ride_history_model.dart';
import 'package:routes_passenger_app/core/viewmodels/select_payment_method_model.dart';

import 'core/services/api.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerFactory(() => PhoneNumberModel());
  locator.registerFactory(() => PhoneNumberOtpModel());
  locator.registerFactory(() => SignUpModel());
  locator.registerFactory(() => HomeModel());
  locator.registerFactory(() => ProfileModel());
  locator.registerFactory(() => PromoModel());
  locator.registerFactory(() => ReferModel());
  locator.registerFactory(() => EnterAddressModel());
  locator.registerFactory(() => RideHistoryModel());
  locator.registerFactory(() => AddHomeWorkModel());
  locator.registerFactory(() => SelectPaymentMethodModel());
  locator.registerFactory(() => AddPaymentCardModel());
  locator.registerLazySingleton(() => Api());
  locator.registerLazySingleton(() => AuthenticationService());
  locator.registerLazySingleton(() => TripService());
  locator.registerLazySingleton(() => UserService());
  locator.registerLazySingleton(() => LocationService());
  locator.registerLazySingleton(() => PromoService());
  locator.registerLazySingleton(() => PaymentService());
}