import 'package:flutter/material.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/ui/views/home_view.dart';
import 'package:routes_passenger_app/ui/views/onboarding_view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:routes_passenger_app/ui/router.dart';
import 'locator.dart';

void main() {
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  var userService = locator<UserService>();
  Future<User> getUser() async{
    var prefs = await SharedPreferences.getInstance();
    String token =  prefs.getString("user_token");
    if (token == null){
      return null;
    }

    return new User(Utilities.getJWTPayload(token));
  }
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Routes',
      debugShowCheckedModeBanner: false,
      onGenerateRoute: Router.generateRoute,
//      routes: Router.getRoutes(),
      home: FutureBuilder<User>(
          future: userService.loadUser(),
          builder: (BuildContext context, AsyncSnapshot<User> snapshot){
            print("user ready");
            print(snapshot.data);
            if (snapshot.hasData){
              if (snapshot.data != null){
                return HomeView();
              } else {
                return OnboardingView();
              }
            }
            return  OnboardingView();
          }
      ),
      theme: ThemeData(
          fontFamily: 'Helvetica Neue',
          primaryColor: Color(0xff2A4467),
          accentColor: Color(0xffF26F22),
//          backgroundColor: Color(0xff1F62CC).withOpacity(0.2),
          scaffoldBackgroundColor: Colors.white,
          textTheme: TextTheme(
            headline: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
            title: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Color(0xff4A72A4)),
            display1: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, letterSpacing: 0.2, color: Color(0xff303030)),
            body1: TextStyle(fontSize: 14.0, color: Color(0xff303030), letterSpacing: 0.2, fontWeight: FontWeight.w300),
          )
      ),
    );
  }
}
