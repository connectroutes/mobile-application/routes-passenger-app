
import 'package:flutter/material.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/locator.dart';

class DrawerWidget extends StatelessWidget{

  var _userService = locator<UserService>();
  List<String> items = ["Profile", "Payments", "Ride History", "Refer a Friend", "Promo Codes", "Customer Support", "About Routes"];

  @override
  Widget build(BuildContext context) {
    return SizedBox(
//      width: MediaQuery.of(context).size.width,
      child: Drawer(
        child: Container(
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.only(left: 15),
          color: Theme.of(context).primaryColor,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 130,
                child: DrawerHeader(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          ProfilePicture(size: 30,),
                          Padding(padding: EdgeInsets.only(left: 20),),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(_userService.getCurrentUser().getName(), style: TextStyle(fontSize: 18, color: Colors.white),),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Icon(Icons.star, color: Color(0xfff26f22), size: 11,),
                                  Text(_userService.getCurrentUser().getAverageRating().toString(), style: TextStyle(color: Colors.white, fontSize: 13), )
                                ],
                              )
                            ],
                          ),



                        ],
                      ),
                     /* IconButton(icon: Icon(Icons.close, color: Colors.white,), onPressed: (){
                        Navigator.of(context).pop();
                      },)*/
                    ],
                  ),
                ),
              ),
              ...new List<Widget>.generate(items.length, (int index){
                return Padding(
                  padding: const EdgeInsets.only(top: 26),
                  child: InkWell(
                    onTap: (){
                      Navigator.of(context).pop();
                      Navigator.pushNamed(context, items[index]);
                    },
                    child: Container(
                        child: Text(items[index], style: TextStyle(fontSize: 18, color: Colors.white))),
                  ),
                );
              }),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Divider(),
                  )
                ],
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.bottomLeft,
                  child: Padding(
                    padding: const EdgeInsets.all(20),
                    child: Text("Make Money Driving", style: TextStyle(color: Colors.white),),
                  ),
                ),
              ),
            ],
          )
        ),
      ),
    );
  }
}