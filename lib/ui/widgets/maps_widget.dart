import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:routes_app_lib/models/models.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/locator.dart';


typedef void MapWidgetCreatedCallback(MapsController controller);

class MapsController{
  _MapsWidgetState state;
  MapsController(this.state);

  goToCurrentLocation(){
    state.getAndUpdateCurrentLocation(cached: false);
  }
}
class MapsWidget extends StatefulWidget{
  MapWidgetCreatedCallback callback;
  var _state = _MapsWidgetState();

  MapsWidget(this.callback){
    MapsController mapsController = new MapsController(_state);
    callback(mapsController);
  }
  @override
  _MapsWidgetState createState() => _state;
}

class _MapsWidgetState extends State<MapsWidget> {


  GoogleMapController googleMapController;
  Location location;

  var locationService = locator<LocationService>();

  getAndUpdateCurrentLocation({cached = true})async{
    location = await locationService.getCurrentLocation(cached: cached);
    googleMapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
      target: LatLng(location.latitude, location.longitude),
      zoom: 18,
    )));

  }
  @override
  Widget build(BuildContext context) {

    return GoogleMap(
      myLocationEnabled: true,
      myLocationButtonEnabled: false,
      mapType: MapType.normal,
//      markers: _createMarker(),
//      polylines: Set<Polyline>.of(_mapPolylines.values),
      initialCameraPosition: CameraPosition(
        zoom: 18,
        target: locationService.location == null
            ? LatLng(6.5244, 3.3792) //lagos
            : LatLng(locationService.location.latitude, locationService.location.longitude),
      ),
      onMapCreated: (GoogleMapController controller) {
        googleMapController = controller;
        if (locationService.location == null){
          getAndUpdateCurrentLocation();
        }
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
  }


}