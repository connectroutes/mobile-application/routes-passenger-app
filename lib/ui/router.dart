import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:routes_passenger_app/ui/views/about_routes_view.dart';
import 'package:routes_passenger_app/ui/views/add_home_work.dart';
import 'package:routes_passenger_app/ui/views/add_payment_card.dart';
import 'package:routes_passenger_app/ui/views/authentication/phone_number_otp_view.dart';
import 'package:routes_passenger_app/ui/views/authentication/phone_number_view.dart';
import 'package:routes_passenger_app/ui/views/authentication/signup_success.dart';
import 'package:routes_passenger_app/ui/views/authentication/signup_view.dart';
import 'package:routes_passenger_app/ui/views/customer_support_view.dart';
import 'package:routes_passenger_app/ui/views/edit_profile_view.dart';
import 'package:routes_passenger_app/ui/views/enter_address_view.dart';
import 'package:routes_passenger_app/ui/views/home_view.dart';
import 'package:routes_passenger_app/ui/views/onboarding_view.dart';
import 'package:routes_passenger_app/ui/views/profile_view.dart';
import 'package:routes_passenger_app/ui/views/promo_view.dart';
import 'package:routes_passenger_app/ui/views/refer_view.dart';
import 'package:routes_passenger_app/ui/views/ride_history_view.dart';
import 'package:routes_passenger_app/ui/views/select_payment_method_view.dart';

const String initialRoute = "login";

class Router {

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case 'onboarding':
        return MaterialPageRoute(builder: (_) => OnboardingView());
      case 'home':
        return MaterialPageRoute(builder: (_) => HomeView());
      case 'login_phone_number_view':
        return CupertinoPageRoute(builder: (_) => LoginPhoneNumberView());
      case 'login_phone_number_otp_view':
        return CupertinoPageRoute(builder: (_) => LoginPhoneNumberOtpView(settings.arguments as String));
      case 'signup_view':
        return CupertinoPageRoute(builder: (_) => SignUpView(settings.arguments as SignUpViewArguments));
      case 'signup_success':
        return CupertinoPageRoute(builder: (_) => SignUpSuccess());
      case "add_home_work":
        return CupertinoPageRoute(builder: (_) => AddHomeWorkView(settings.arguments as String));
      case "add_payment_card":
        return CupertinoPageRoute(builder: (_) => AddPaymentCardView());
      case 'Profile':
        return CupertinoPageRoute(builder: (_) => ProfileView());
      case 'About Routes':
        return CupertinoPageRoute(builder: (_) => AboutRoutesView());
      case 'Customer Support':
        return CupertinoPageRoute(builder: (_) => CustomerSupportView());
      case 'Promo Codes':
        return CupertinoPageRoute(builder: (_) => PromoView());
      case 'Refer a Friend':
        return CupertinoPageRoute(builder: (_) => ReferView());
      case 'edit_profile':
        return CupertinoPageRoute(builder: (_) => EditProfileView());
      case 'enter_address':
        return CupertinoPageRoute(builder: (_) => EnterAddressView());
      case "Ride History":
        return CupertinoPageRoute(builder: (_) => RideHistoryView());
      case "Payments":
        return CupertinoPageRoute(builder: (_) => SelectPaymentMethodView());
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
              body: Center(
                child: Text('No route defined for ${settings.name}'),
              ),
            ));
    }
  }
}