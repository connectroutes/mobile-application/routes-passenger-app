import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/services/generated/payment.pbgrpc.dart';
import 'package:routes_passenger_app/core/viewmodels/profile_model.dart';
import 'package:routes_passenger_app/core/viewmodels/select_payment_method_model.dart';
import 'package:routes_passenger_app/ui/views/base_view.dart';

class SelectPaymentMethodView extends StatefulWidget {
  @override
  _SelectPaymentMethodViewState createState() =>
      _SelectPaymentMethodViewState();
}

class _SelectPaymentMethodViewState extends State<SelectPaymentMethodView>  with WidgetsBindingObserver{
  AnimationController animationController;

  Animation<Offset> animationOffset;


  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }
  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  //TODO remove if not needed
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      //do your stuff
    }
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<SelectPaymentMethodModel>(
      builder: (context, model, child) => Scaffold(
        body: Container(
          margin: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top,
          ),
          padding: EdgeInsets.only(left: 22, top: 10, right: 22),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              RoutesAppBar(
                "Payments",
              ),
              Padding(
                padding: EdgeInsets.only(top: 34),
              ),
              Text("Payment Methods"),
              Flexible(
                child: ListView.separated(
                    separatorBuilder: (context, index) {
                      return Divider();
                    },
                    itemBuilder: (BuildContext context, int index) {

                      if (index == model.getOptions().length){
                        return  InkWell(
                          onTap: () {
                            Navigator.of(context).pushNamed("add_payment_card");
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Icon(Icons.add),
                              Padding(
                                padding: EdgeInsets.only(left: 30),
                              ),
                              Text("Add Payment Card")
                            ],
                          ),
                        );
                      }

                      PaymentOption option = model.getOptions()[index];

                      return InkWell(
                        onTap: (){
                          model.setPaymentOption(option.id);
                        },
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                option.method == PaymentMethod.cash ? Image.asset("assets/cash.png") : option.cardType == CardType.mastercard ?  Image.asset("assets/mastercard_icon.png") :  Image.asset("assets/visa_icon.png"),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(option.method == PaymentMethod.cash ? "Cash" :  "**** "+option.cardLast4),
                              ],
                            ),
                            Checkbox(value: model.getSelectedPaymentOption() == option.id, onChanged: (value){

                            },)

                          ],
                        ),
                      );
                    },
                    itemCount: model.getOptions().length + 1),// the extra 1 is for adding a card
              ),
            ],
          ),
        ),
      ),
    );
  }
}
