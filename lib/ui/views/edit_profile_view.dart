import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/viewmodels/profile_model.dart';
import 'package:routes_passenger_app/ui/views/base_view.dart';

class EditProfileView extends StatefulWidget{
  @override
  _EditProfileViewState createState() => _EditProfileViewState();
}

class _EditProfileViewState extends State<EditProfileView>  with SingleTickerProviderStateMixin  {
  AnimationController animationController;

  Animation<Offset> animationOffset;


  @override
  void initState() {
    super.initState();
    animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    animationOffset = Tween<Offset>(begin: Offset(0.0, 1.0), end: Offset.zero)
        .animate(animationController);
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<ProfileModel>(
      builder: (context, model, child) => Scaffold(
        body: Container(
          margin: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top,
          ),
          padding: EdgeInsets.only(left: 22, top: 10, right: 22),
          child: Column(
            children: <Widget>[
              RoutesAppBar(
                "Edit Profile",
              ),
              Padding(
                padding: EdgeInsets.only(top: 34),
              ),
              ...buildWidgets(context, model),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> buildWidgets(BuildContext context, ProfileModel model){
    return [
      ...buildProfileDetails(model, context),
    ];
  }

  buildAddPhoto(BuildContext context){
    return Stack(
      children: <Widget>[
        Container(
          height: 100,
          width: 70,
//                    color: Colors.white,
        ),
        Positioned(
          top: 10,
          child: Container(
            width: 70,
            height: 70,
            decoration: BoxDecoration(
                shape: BoxShape.circle, color: Color(0xffC4C4C4)),
          ),
        ),
        Positioned(
          top: 28,
          left: 15,
          child: Text(
            "Add\nphoto",
            textAlign: TextAlign.center,
          ),
        ),
        Positioned(
            top: -7,
            right: 18,
            child: CircleAvatar(
                radius: 15,
                backgroundColor:
                Theme.of(context).scaffoldBackgroundColor,
                child: Image.asset(
                  "assets/camera.png",
                  width: 15,
                  height: 15,
                )))
      ],
    );
  }

  List<Widget> buildProfileDetails(ProfileModel model, BuildContext context){

    if (model.editingField != null){
      return buildEditingField(model);
    }

    return [
      buildAddPhoto(context),
      GestureDetector(
        onTap: (){
          animationController.forward();
          model.editField("name");
        },
        child: RoutesTextField(
          model.fullNameController,
          label: "Full Name",
          readOnly: true,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(top: 20),
      ),
      GestureDetector(
        onTap: (){
          animationController.forward();
          model.editField("email");
        },
        child: RoutesTextField(
          model.emailController,
          label: "Email",
          readOnly: true,
          suffix: Padding(
            padding: const EdgeInsets.only(right: 5.0),
            child: Text(
              "Verified",
              style: TextStyle(color: Color(0xff6D9933), fontSize: 12),
            ),
          ),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(top: 20),
      ),
      GestureDetector(
        onTap: (){
          animationController.forward();
          model.editField("phone");
        },
        child: RoutesTextField(
          model.mobileNumberController,
          readOnly: true,
          label: "Mobile Number",
          suffix: Padding(
            padding: const EdgeInsets.only(right: 5.0),
            child: Text(
              "Verified",
              style: TextStyle(color: Color(0xff6D9933), fontSize: 12),
            ),
          ),
        ),
      ),
    ];
  }

  List<Widget> buildEditingField(ProfileModel model){
    return [
      buildEditingTextField(model),
      Padding(padding: EdgeInsets.only(top: 40),),
      Button("Save", (){

      }),
    ];
  }

  Widget buildEditingTextField(ProfileModel model){
    if (model.editingField == "name"){
      return SlideTransition(position: animationOffset, child: RoutesTextField(model.fullNameController, label: model.editingFieldDescription,));
    } else if (model.editingField == "email"){
      return SlideTransition(position: animationOffset, child: RoutesTextField(model.fullNameController, textInputType: TextInputType.emailAddress, label: model.editingFieldDescription,));
    }

    return SlideTransition(position: animationOffset, child: PhoneNumberInputTextField(model.mobileNumberController));
  }
}
