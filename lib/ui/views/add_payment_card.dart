import 'dart:async';

import 'package:flutter/material.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/viewmodels/add_payment_card_model.dart';
import 'package:awesome_card/awesome_card.dart';
import 'package:routes_passenger_app/ui/views/base_view.dart';
import 'package:webview_flutter/webview_flutter.dart';

class AddPaymentCardView extends StatefulWidget{
  @override
  _AddPaymentCardViewState createState() => _AddPaymentCardViewState();
}

class _AddPaymentCardViewState extends State<AddPaymentCardView>{

  final Completer<WebViewController> _controller =
  Completer<WebViewController>();

  @override
  Widget build(BuildContext context) {
    return BaseView<AddPaymentCardModel>(
      builder: (context, model, child) => Scaffold(
        body: Container(
          margin: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top,
          ),
          padding: EdgeInsets.only(left: 11, top: 10, right: 11),
          child: LayoutBuilder(builder: (context, constraint){
            return SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(minHeight: constraint.maxHeight),
                child: IntrinsicHeight(
                  child: Column(
                    children: <Widget>[
                      RoutesAppBar(
                        "Add Payment Card",
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 34),
                      ),
                      ..._buildBody(model)
                    ],
                  ),
                ),
              ),
            );
          },),
        ),
      ),
    );
  }

  List<Widget> _buildBody(AddPaymentCardModel model){
    if (model.action == ""){
      return _buildCollectCardDetails(model);
    } else if (model.action == "send_otp"){
      return _buildCollectOtp(model);
    } else if (model.action == "open_url"){
      return [
        Expanded(
          child: WebView(
            initialUrl: model.actionUrl,
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController webViewController) {
              _controller.complete(webViewController);
            },
            onPageStarted: (String url) {
              print('Page started loading: $url');

              if (url.contains("close")){
                //close window
                model.addCardFromReference();
              }
            },
            onPageFinished: (String url) {
              print('Page finished loading: $url');
            },
            gestureNavigationEnabled: false,
          ),
        )
      ];
    } else if (model.action == "success"){
      Utilities.showToast(context, "Card Added");
      Navigator.pop(context);
      return [];
    }

    return [];
  }

  List<Widget> _buildCollectOtp(AddPaymentCardModel model){
    return [
      Text(model.actionMessage),
      Container(
        margin: EdgeInsets.symmetric(horizontal: 20,),
        child: TextFormField(
          decoration: InputDecoration(hintText: "OTP"),
          onChanged: (value) {
            model.actionValue = value;
          },
        ),
      ),
      SizedBox(
        height: 10,
      ),
      Button("Submit", (){
        model.submitOtp();
      }, loading: model.loading,),
    ];
  }

  List<Widget> _buildCollectCardDetails(AddPaymentCardModel model){
    return [

      CreditCard(
        cardNumber: model.cardNumber,
        cardExpiry: model.expiryDate,
        cardHolderName: model.cardHolderName,
        cvv: model.cvv,
        bankName: model.bankName,
        showBackSide: model.showBackSide,
        frontBackground: new Container(
          width: double.maxFinite,
          height: double.maxFinite,
          color: Theme.of(context).primaryColor,
        ),
        backBackground: CardBackgrounds.white,
//        showShadow: true,
      ),
      SizedBox(
        height: 40,
      ),
      model.error != null ? Text(model.error, style: TextStyle(color: Colors.red),) : Container(),
      Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(
              horizontal: 20,
            ),
            child: TextFormField(
              keyboardType: TextInputType.number,
              decoration: InputDecoration(hintText: "Card Number"),
              maxLength: 19,
              onChanged: (value) {
                setState(() {
                  model.cardNumber = value;
                });
              },
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(
              horizontal: 20,
            ),
            child: TextFormField(
              decoration: InputDecoration(hintText: "Card Expiry"),
              maxLength: 5,
              onChanged: (value) {
                setState(() {
                  model.expiryDate = value;
                });
              },
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            child: TextFormField(
              decoration: InputDecoration(hintText: "CVV"),
              maxLength: 3,
              onChanged: (value) {
                setState(() {
                  model.cvv = value;
                });
              },
              focusNode: model.cvvFocusNode,
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20,),
            child: TextFormField(
              keyboardType: TextInputType.number,
              obscureText: true,
              decoration: InputDecoration(hintText: "Card Pin"),
              maxLength: 4,
              onChanged: (value) {
                model.pin = value;
              },
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Button("Add Card", (){
            model.addCard();
          }, loading: model.loading,),
        ],
      )
    ];
  }
}
