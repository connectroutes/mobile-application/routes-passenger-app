import 'package:flutter/material.dart';

class AboutRoutesView extends StatelessWidget {



  @override
  Widget build(BuildContext context) {
//    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);

    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top,),
        padding: EdgeInsets.only(left: 22, top: 10, right: 22),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    IconButton(icon: Icon(Icons.arrow_back), onPressed: (){
                      Navigator.pop(context);
                    },),
                    Padding(padding: EdgeInsets.only(left: 20),),
                    Text("About Routes", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                  ],
                ),
              ],
            ),
            Padding(padding: EdgeInsets.only(top: 43),),
            Text("Routes​ is a carpooling mobile application that seamlessly connects passengers to drivers going to the same destination.\n\n Routes also connects passengers to drivers that are not exactly going to their destination but will pass passenger’s destination." ,
            textAlign: TextAlign.left,),
            Padding(padding: EdgeInsets.only(top: 24),),
            Text("Mission", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
            Padding(padding: EdgeInsets.only(top: 24),),
            Text("Our mission is to constantly provide safe, affordable, timely and convenient rides for passengers while providing an extra means of income for the drivers and ensuring the driver’s are not inconvenienced.\n\n At Routes, we believe in ease of transportation and constantly try to make this a reality for all our customers. We have an amazing team with bright minds who everyday find new ways to make your journey seamless using the best routes. Try us today!" ,
              textAlign: TextAlign.left,),
          ],
        ),
      ),
    );

  }

}
