import 'package:flutter/material.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/services/generated/trip.pb.dart';
import 'package:routes_passenger_app/core/viewmodels/add_home_work_model.dart';
import 'package:routes_passenger_app/core/viewmodels/enter_address_model.dart';
import 'package:routes_passenger_app/core/viewmodels/promo_model.dart';
import 'package:routes_passenger_app/ui/views/base_view.dart';

class AddHomeWorkView extends StatefulWidget {
  String type;
  AddHomeWorkView(this.type);
  @override
  _AddHomeWorkViewState createState() => _AddHomeWorkViewState();
}

class _AddHomeWorkViewState extends State<AddHomeWorkView> {
  @override
  Widget build(BuildContext context) {
    return BaseView<AddHomeWorkModel>(
      builder: (context, model, child) => Scaffold(
        body: Container(
          margin: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top,
          ),
          padding: EdgeInsets.only(left: 22, top: 10, right: 22),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              RoutesAppBar(
                "Add ${widget.type == "home" ? "Home" : "Work"}",
              ),
              Padding(
                padding: EdgeInsets.only(top: 26),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 2, bottom: 2, right: 2),
                    width: 8,
                    height: 8,
                    decoration: BoxDecoration(
                      color: Theme.of(context).accentColor,
                      borderRadius: BorderRadius.circular(8),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 10),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width - 65,
                    child: AddressTextField(
                      model.addressController,
                      "To",
                      "Enter ${widget.type} address",
                    ),
                  )
                ],
              ),
              Flexible(
                child: ListView.separated(
                    separatorBuilder: (context, index) {
                      return Divider();
                    },
                    itemCount: model.suggestions.length,
                    itemBuilder: (BuildContext context, int index) {
                      var fullDescription =
                          model.suggestions[index].description;
                      String title, description;
                      if (fullDescription.contains(",")) {
                        title = fullDescription.split(",")[0];
                        description = fullDescription.split(",")[1];
                      } else {
                        title = description = fullDescription;
                      }
                      return Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () {
                            model.setDefaultAddress(description, model.suggestions[index].placeId, widget.type == "home" ? AddressType.home : AddressType.work);
                            Navigator.pop(context);
                          },
                          child: Row(
                            children: <Widget>[
                              Center(
                                  child: Image(
                                image: AssetImage("assets/clock.png"),
                                width: 22,
                                height: 22,
                              )),
                              Padding(
                                padding: EdgeInsets.only(left: 25),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(top: 15),
                                  ),
                                  Text(
                                    title,
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .textTheme
                                            .display1
                                            .color,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 5),
                                  ),
                                  Text(description),
                                  Padding(
                                    padding: EdgeInsets.only(top: 15),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      );
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }
}
