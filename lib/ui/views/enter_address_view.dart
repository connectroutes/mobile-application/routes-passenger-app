import 'package:flutter/material.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/viewmodels/enter_address_model.dart';
import 'package:routes_passenger_app/core/viewmodels/promo_model.dart';
import 'package:routes_passenger_app/ui/views/base_view.dart';

class EnterAddressView extends StatefulWidget {
  @override
  _EnterAddressViewState createState() => _EnterAddressViewState();
}

class _EnterAddressViewState extends State<EnterAddressView> {
  @override
  Widget build(BuildContext context) {
    return BaseView<EnterAddressModel>(
      builder: (context, model, child) => Scaffold(
        body: Container(
          margin: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top,
          ),
          padding: EdgeInsets.only(left: 22, top: 10, right: 22),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              RoutesAppBar(
                "Drop off Location",
              ),
              Padding(
                padding: EdgeInsets.only(top: 26),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  AddressCircles(9),
                  Padding(
                    padding: EdgeInsets.only(left: 10),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          width: MediaQuery.of(context).size.width - 65,
                          child: AddressTextField(model.pickupController,
                              "From", "Search for pickup location", focusNode: model.pickupFocusNode,), ),
                      Container(
                          width: MediaQuery.of(context).size.width - 65,
                          child: AddressTextField(model.destinationController,
                              "To", "Search for drop off location", focusNode: model.destinationFocusNode,)),
                    ],
                  )
                ],
              ),
              Flexible(
                child: ListView.separated(
                    separatorBuilder: (context, index) {
                      return Divider();
                    },
                    itemCount: model.suggestions.length,
                    itemBuilder: (BuildContext context, int index) {
                      var fullDescription = model.suggestions[index].description;
                      String title, description;
                      if (fullDescription.contains(",")){
                        title = fullDescription.split(",")[0];
                        description = fullDescription.split(",")[1];
                      } else {
                        title = description = fullDescription;
                      }
                      return Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: (){print("tapped");},
                          child: Row(
                            children: <Widget>[
                              Center(child: Image(image: AssetImage("assets/clock.png"), width: 22, height: 22,)),
                              Padding(padding: EdgeInsets.only(left: 25),),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(padding: EdgeInsets.only(top: 15),),
                                  Text(title, style: TextStyle(color: Theme.of(context).textTheme.display1.color, fontSize: 14, fontWeight: FontWeight.w500),),
                                  Padding(padding: EdgeInsets.only(top: 5),),
                                  Text(description),
                                  Padding(padding: EdgeInsets.only(top: 15),),

                                ],
                              )

                            ],
                          ),
                        ),
                      );
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }
}
