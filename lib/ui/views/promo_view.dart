import 'package:flutter/material.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/viewmodels/promo_model.dart';
import 'package:routes_passenger_app/ui/views/base_view.dart';

class PromoView extends StatefulWidget {
  @override
  _PromoViewState createState() => _PromoViewState();
}

class _PromoViewState extends State<PromoView> {
  @override
  Widget build(BuildContext context) {
    return BaseView<PromoModel>(
      builder: (context, model, child) => Scaffold(
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(
              top: MediaQuery.of(context).padding.top,
            ),
            padding: EdgeInsets.only(left: 22, top: 10, right: 22),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                RoutesAppBar(
                  "Promo Code",
                ),
                Padding(
                  padding: EdgeInsets.only(top: 43),
                ),
                Text("Enter the Promo code to enjoy cheaper rides"),
                Padding(
                  padding: EdgeInsets.only(top: 37),
                ),
                Center(
                  child: new Image.asset(
                    "assets/promo_gift.png",
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 37),
                ),
                RoutesTextField(
                  model.promoCodeController,
                  label: "Promo Code",
                  hint: "Enter Promo Code",
                  dottedBorder: true,
                  height: 80,
                  textAlign: TextAlign.center,
                  hintFontWeight: FontWeight.bold,
                  textStyle: TextStyle(fontWeight: FontWeight.bold),
                  loading: model.submittingPromoCode,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 33),
                ),
                Button(
                  "Apply",
                  () {
                    model.submitPromoCode();
                  },
                  disabled: !model.inputValid,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
