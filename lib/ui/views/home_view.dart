import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/viewmodels/home_model.dart';
import 'package:routes_passenger_app/ui/views/base_view.dart';
import 'package:routes_passenger_app/ui/widgets/drawer.dart';
import 'package:routes_passenger_app/ui/widgets/maps_widget.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Theme.of(context).primaryColor));

    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>();

    var deviceHeight = MediaQuery.of(context).size.height;
    var deviceWidth = MediaQuery.of(context).size.width;
    double statusBarHeight = MediaQuery.of(context).padding.top;

    double bottomSheetHeight = 310;

    MapsController mapsController;

    return BaseView<HomeModel>(
      builder: (context, model, child) => Scaffold(
        key: _scaffoldKey,
        body: SafeArea(
          child: Column(
            children: <Widget>[
              Container(
                height: 83,
                padding: EdgeInsets.only(left: 10),
                child: Row(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        _scaffoldKey.currentState.openDrawer();
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: Image.asset(
                          "assets/menu.png",
                          height: 40,
                          width: 50,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: RichText(
                        textAlign: TextAlign.left,
                        text: new TextSpan(
                          children: [
                            new TextSpan(
                              text: model.helloText + "\n",
                              style: new TextStyle(
                                color: Theme.of(context).textTheme.display1.color,
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                              ),
                            ),
                            new TextSpan(
                              text: 'Where are you going?',
                                style: Theme.of(context).textTheme.body1,
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Stack(
                children: <Widget>[
                  SizedBox(
                    width: double.infinity,
                    height: deviceHeight - statusBarHeight - 117,
                    child: MapsWidget((MapsController _controller) {
                      mapsController = _controller;
                    }),
                  ),
                  Positioned(
                    bottom: 0.0,
                    child: Container(
                      padding: EdgeInsets.only(left: 20),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        boxShadow: [
                          BoxShadow(
                            color: Color(0x1F62CC29),
                            blurRadius: 15.0, // has the effect of softening the shadow
//                            spreadRadius: 5.0, // has the effect of extending the shadow

                          )
                        ],
                      ),
                      height: bottomSheetHeight,
                      width: deviceWidth- 20,
                      margin: EdgeInsets.only(left: 10),
                      child: Column(
//            mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 25),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              AddressCircles(9),
                              Padding(padding: EdgeInsets.only(left: 10),),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  _buildAddressInfo(model, "From", model.pickupAddress, "Search for pickup location"),
                                  Container( width: MediaQuery.of(context).size.width - 80, child: Divider()),
                                  _buildAddressInfo(model, "To", model.destinationAddress, "Search for drop off location"),

                                ],
                              )
                            ],
                          ),
                          _buildHomeOrWork("Home", "assets/home.png", () {}),
                          Divider(),
                          _buildHomeOrWork("Work", "assets/briefcase.png", () {}),
                          Padding(
                            padding: EdgeInsets.only(top: 20),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
        floatingActionButton: Padding(
          padding: EdgeInsets.only(bottom: bottomSheetHeight + 35),
          child: new FloatingActionButton(
            child: Icon(Icons.my_location),
            backgroundColor: Theme.of(context).primaryColor,
            onPressed: () {
              mapsController.goToCurrentLocation();
            },
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        drawer: DrawerWidget(),
      ),
    );
  }


  _buildAddressInfo(HomeModel model, String label, String address, String hint){
      return GestureDetector(
        onTap: (){
          print("tap tap");
          Navigator.of(context).pushNamed("enter_address");
        },
        child: Container(
          width: MediaQuery.of(context).size.width - 80,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(label),
                  Padding(padding: EdgeInsets.only(top: 5),),
                  address.isEmpty ? Text(hint, style: TextStyle(color: Theme.of(context).textTheme.display1.color.withOpacity(0.48)),) : Text(address, style: TextStyle( fontWeight: FontWeight.bold),)

                ],
              ),
              IconButton(
                onPressed: (){

                },
                icon: Icon(
                  model.pickupAddress.isEmpty ? Icons.search : Icons.close,
                  color: Color(0xFFD8D8D9),
                ),
              )
            ],
          ),
        ),
      );
  }
  _buildHomeOrWork(String text, String assetName, VoidCallback onTap) {
    return GestureDetector(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Image(image: AssetImage(assetName), width: 24, height: 24,),
                Padding(
                  padding: EdgeInsets.only(left: 15),
                  child: Text(text),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(milliseconds: 500), () {
//  TODO    Utilities.showLocationPermissionDialog(context);
    });


  }
}
