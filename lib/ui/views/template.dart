import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/viewmodels/profile_model.dart';
import 'package:routes_passenger_app/ui/views/base_view.dart';

class SampleView extends StatefulWidget{
  @override
  _SampleViewState createState() => _SampleViewState();
}

class SampleStatelessView extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return BaseView<ProfileModel>(
      builder: (context, model, child) => Scaffold(
        body: Container(
          margin: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top,
          ),
          padding: EdgeInsets.only(left: 22, top: 10, right: 22),
          child: Column(
            children: <Widget>[
              RoutesAppBar(
                "Edit Profile",
              ),
              Padding(
                padding: EdgeInsets.only(top: 34),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
class _SampleViewState extends State<SampleView>{
  AnimationController animationController;

  Animation<Offset> animationOffset;


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<ProfileModel>(
      builder: (context, model, child) => Scaffold(
        body: Container(
          margin: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top,
          ),
          padding: EdgeInsets.only(left: 22, top: 10, right: 22),
          child: Column(
            children: <Widget>[
              RoutesAppBar(
                "Edit Profile",
              ),
              Padding(
                padding: EdgeInsets.only(top: 34),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
