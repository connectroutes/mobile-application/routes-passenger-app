import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/viewmodels/profile_model.dart';
import 'package:routes_passenger_app/ui/views/base_view.dart';

class ProfileView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>();

    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);

    return BaseView<ProfileModel>(
      builder: (context, model, child) => Scaffold(
        key: _scaffoldKey,
        body: Container(
          margin: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top,
          ),
          padding: EdgeInsets.only(left: 22, top: 10, right: 22),
          child: Column(
            children: <Widget>[
              RoutesAppBar(
                "Profile",
                rightWidget: IconButton(
                  icon: Icon(Icons.edit),
                  color: Theme.of(context).primaryColor,
                  onPressed: () {
                    Navigator.of(context).pushNamed("edit_profile");
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 34),
              ),
              Center(
                child: ProfilePicture(),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
              ),
              Center(
                child: Text(
                  model.user.getName(),
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 25),
              ),
              Divider(),
              Padding(
                padding: EdgeInsets.only(top: 25),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Text(
                        model.user.getTotalRides().toString(),
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.bold),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 5),
                      ),
                      Text("Total rides")
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Color(0xfff26f22),
                            size: 11,
                          ),
                          Text(
                            model.user.getAverageRating().toString(),
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 5),
                      ),
                      Text("Rating")
                    ],
                  )
                ],
              ),
              Padding(
                padding: EdgeInsets.only(top: 25),
              ),
              Divider(),
              _buildProfileAction(
                  Icon(Icons.home), "Add Home", model.user.getHomeAddress(),
                  () {
                Navigator.pushNamed(context, "add_home_work",
                    arguments: "home");
              }),
              Divider(),
              _buildProfileAction(
                  Icon(Icons.work), "Add Work", model.user.getWorkAddress(),
                  () {
                Navigator.pushNamed(context, "add_home_work",
                    arguments: "work");
              }),
              Divider(),
              _buildProfileAction(
                  Icon(Icons.power_settings_new), "Log Out", null, () {
                model.logOut();
                Navigator.pushNamedAndRemoveUntil(
                    context, "login_phone_number_view", (r) => false);
              }),
              Divider(),
            ],
          ),
        ),
      ),
    );
  }

  _buildProfileAction(
      Icon icon, String text, String summary, VoidCallback onTap) {
    return Container(
      height: 60,
//      padding: EdgeInsets.only(top: 25, bottom: 25),
      child: InkWell(
        onTap: onTap,
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                icon,
                Padding(
                  padding: EdgeInsets.only(left: 25),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(text),
                    summary == null
                        ? Container()
                        : SizedBox(
                      height: 10,
                    ),
                    summary == null
                        ? Container()
                        : Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Icon(
                                Icons.access_time,
                                size: 12,
                                color: Colors.grey,
                              ),
                              Text(
                                summary,
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Color(0xFF303030).withOpacity(0.76)),
                              ),
                            ],
                          )
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
