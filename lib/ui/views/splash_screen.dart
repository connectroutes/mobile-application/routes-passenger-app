import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget{

  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: Center(
        child: Image.asset("assets/routes_logo.png"),
      ),
    );
  }
}