import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/services/generated/trip.pbgrpc.dart';
import 'package:routes_passenger_app/core/viewmodels/profile_model.dart';
import 'package:routes_passenger_app/core/viewmodels/ride_history_model.dart';
import 'package:routes_passenger_app/ui/views/base_view.dart';

class RideHistoryView extends StatefulWidget {
  @override
  _RideHistoryViewState createState() => _RideHistoryViewState();
}

class _RideHistoryViewState extends State<RideHistoryView> {
  AnimationController animationController;

  Animation<Offset> animationOffset;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<RideHistoryModel>(
      builder: (context, model, child) => Scaffold(
        body: Container(
          margin: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top,
          ),
          padding: EdgeInsets.only(left: 22, top: 10, right: 22),
          child: Column(
            children: <Widget>[
              RoutesAppBar(
                "Ride History",
              ),
              Expanded(
                child: ListView.builder(
                  itemBuilder: (BuildContext context, int index) {
                    return RideCard(model.trips[index]);
                  },
                  itemCount: model.trips.length,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class RideCard extends StatelessWidget {
  Trip trip;
  RideCard(this.trip);
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          boxShadow: [
            BoxShadow(
              color: Color(0x1F62CC24),
              blurRadius: 10.0, // has the effect of softening the shadow
            )
          ],
        ),
        padding: EdgeInsets.all(19),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                AddressCircles(9),
                Padding(
                  padding: EdgeInsets.only(left: 10),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "From",
                      style: TextStyle(fontSize: 12),
                    ),
                    Text(
                      "Ifako Bus Stop",
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                    ),
                    Container(
                      height: 20,
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(color: Colors.green, width: 3.0),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                    ),
                    Text(
                      "To",
                      style: TextStyle(fontSize: 12),
                    ),
                    Text(
                      "Ahmed Onibudo Bus Stop",
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                    ),
                  ],
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
            ),
            Divider(),
            Padding(
              padding: EdgeInsets.only(top: 10),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                RichText(
                  text: new TextSpan(
                    children: [
                      new TextSpan(
                        text: "ID: ",
                        style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 10,
                            color: Theme.of(context).textTheme.display1.color),
                      ),
                      new TextSpan(
                        text: trip.id,
                        style: new TextStyle(
                            fontSize: 10,
                            color: Theme.of(context).textTheme.display1.color),
                      ),
                    ],
                  ),
                ),
                Text(
                  getTripStatus(),
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: trip.status == TripStatus.cancelled
                          ? FAILURE_COLOR
                          : SUCCESS_COLOR,
                      fontSize: 10),
                ),
                Text(
                  getTimeText(),
                  style: TextStyle(fontSize: 10),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  String getTripStatus() {
    switch (trip.status) {
      case TripStatus.created:
        return "Created";
      case TripStatus.in_progress:
        return "In Progress";
      case TripStatus.cancelled:
        return "Cancelled";
      case TripStatus.done:
        return "Successful";
    }
    return "Successful";
  }

  String getTimeText() {
    DateTime tripTime =
        DateTime.fromMillisecondsSinceEpoch(trip.requestTime.toInt());
    DateTime now = DateTime.now();
    if (tripTime.day == now.day) {
      return "Today: ${DateFormat('h:mm a').format(tripTime)}"; //TODO do for yesterday
    } else {
      return DateFormat('h:mm a, d MMM, yyyy').format(tripTime);
    }
  }
}
