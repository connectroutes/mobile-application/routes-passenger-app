import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:routes_app_lib/routes_app_lib.dart';

class CustomerSupportView extends StatelessWidget {



  @override
  Widget build(BuildContext context) {
//    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);

    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top,),
        padding: EdgeInsets.only(left: 22, top: 10, right: 22),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    IconButton(icon: Icon(Icons.arrow_back), onPressed: (){
                      Navigator.pop(context);
                    },),
                    Padding(padding: EdgeInsets.only(left: 20),),
                    Text("Customer Support", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                  ],
                ),
              ],
            ),
            Padding(padding: EdgeInsets.only(top: 43),),
            RichText(
              text: new TextSpan(
                children: [
                  new TextSpan(
                    text: "Please send an email to ",
                    style: Theme.of(context).textTheme.body1,
                  ),
                  new TextSpan(
                    text: 'customersupport@connectroutes.com',
                    style: new TextStyle(color: Colors.blue, fontWeight: FontWeight.bold ),
                    recognizer: new TapGestureRecognizer()
                      ..onTap = () {
                        print("tap");
                        Utilities.openUrl("mailto:customersupport@connectroutes.com?subject=Routes App Support");
                      },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );

  }

}
