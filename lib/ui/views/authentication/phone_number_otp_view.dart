import 'package:flutter/material.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/viewmodels/authentication/phone_number_otp_model.dart';
import 'package:routes_passenger_app/locator.dart';
import 'package:routes_passenger_app/ui/views/authentication/signup_view.dart';
import 'package:routes_passenger_app/ui/views/base_view.dart';

class LoginPhoneNumberOtpView extends StatefulWidget {
  String phoneNumber;
  LoginPhoneNumberOtpView(this.phoneNumber);
  @override
  _LoginPhoneNumberOtpState createState() => _LoginPhoneNumberOtpState(phoneNumber);
}

class _LoginPhoneNumberOtpState extends State<LoginPhoneNumberOtpView> {
  String phoneNumber;

  UserService userService = locator<UserService>();
  _LoginPhoneNumberOtpState(this.phoneNumber);

  @override
  Widget build(BuildContext context) {
    return BaseView<PhoneNumberOtpModel>(
      builder: (context, model, widget) => Scaffold(
        body: Container(
          padding: EdgeInsets.only(top: 94, left: 22, right: 22),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Enter the OTP code sent to\n$phoneNumber",
              ),
              new Padding(padding: EdgeInsets.only(top: 36)),
              OtpInputTextField(model.inputChanged),
              new Padding(padding: EdgeInsets.only(top: 33)),
              Button(
                "Confirm Mobile Number",
                () async {
                  var response = await model.submitOtp(phoneNumber);
                  if (response.isNewUser) {
                    Navigator.of(context).pushReplacementNamed("signup_view",
                        arguments: new SignUpViewArguments(
                            phoneNumber, response.otpAuthenticationId));
                  } else {
                    userService.storeUserToken(response.token, response.userData);

                    Navigator.of(context).pushReplacementNamed("home");
                  }
                },
                disabled: !model.inputValid,
              ),
              new Padding(padding: EdgeInsets.only(top: 29)),
              model.resendCodeEnabled
                  ? GestureDetector(
                    onTap: (){
                      model.resendOtp(phoneNumber);
                    },
                    child: Center(
                      child: Text(
                          "Resend Code",
                          style: TextStyle(
                              color: Theme.of(context).textTheme.title.color),
                        ),
                    ),
                  )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("Resend code in "),
                        TextCountDown(30, model.enableResetCountDown)
                      ],
                    ),
              new Padding(padding: EdgeInsets.only(top: 27)),
              GestureDetector(
                onTap: (){
                  Navigator.of(context).pushReplacementNamed("login_phone_number_view");
                },
                child: Center(
                  child: Text(
                    "Change mobile number",
                    style: TextStyle(
                        color: Theme.of(context).textTheme.title.color),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
