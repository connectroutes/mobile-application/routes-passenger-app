import 'package:flutter/material.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/viewmodels/authentication/phone_number_model.dart';
import 'package:routes_passenger_app/ui/views/base_view.dart';

class LoginPhoneNumberView extends StatefulWidget {
  @override
  _LoginPhoneNumberState createState() => _LoginPhoneNumberState();
}


class _LoginPhoneNumberState extends State<LoginPhoneNumberView>{

  @override
  Widget build(BuildContext context) {
    return BaseView<PhoneNumberModel>(
      builder: (context, model, widget) => Scaffold(
        body: Container(
          padding: EdgeInsets.only(top: 94, left: 22, right: 22),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              IconButton(icon: Icon(Icons.close, size: 36,), onPressed: (){
                Navigator.pushReplacementNamed(context, "onboarding");
              },),
              new Padding(padding: EdgeInsets.only(top: 36)),
              Text("Enter your Mobile Number", style: Theme.of(context).textTheme.display1,),
              new Padding(padding: EdgeInsets.only(top: 18)),
              Text("We’ll send an OTP code to verify your mobile\n number"),
              new Padding(padding: EdgeInsets.only(top: 56)),
              PhoneNumberInputTextField(model.phoneNumberController),
              new Padding(padding: EdgeInsets.only(top: 33)),
              Button("Next", (){
                print("next");
                String phoneNumber = '+234${model.phoneNumberController.text}';
                print(phoneNumber);
                model.requestOTP(phoneNumber);
                Navigator.pushReplacementNamed(context, "login_phone_number_otp_view", arguments: phoneNumber );
              }, disabled: !model.inputValid,),
            ],
          ),
        ),
      ),
    );
  }
}