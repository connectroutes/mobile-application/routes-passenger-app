import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/viewmodels/authentication/sign_up_model.dart';
import 'package:routes_passenger_app/ui/views/base_view.dart';

class SignUpViewArguments{
  String phoneNumber, otpAuthenticationId;
  SignUpViewArguments(this.phoneNumber, this.otpAuthenticationId);
}

class SignUpView extends StatefulWidget {
  SignUpViewArguments args;
  SignUpView(this.args);
  @override
  _SignUpState createState() => _SignUpState();
}


class _SignUpState extends State<SignUpView>{

  SignUpView view;
  @override
  Widget build(BuildContext context) {
    view = widget;
    return BaseView<SignUpModel>(
      builder: (context, model, widget) => Scaffold(
        body: Container(
          padding: EdgeInsets.only(top: 94, left: 22, right: 22),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Sign up to continue", style: Theme.of(context).textTheme.display1,),
                new Padding(padding: EdgeInsets.only(top: 36)),
                RoutesTextField(model.nameController, label: "Full Name", hint: "Enter your first name and last name", capitalizeWords: true,),
                new Padding(padding: EdgeInsets.only(top: 20)),
                RoutesTextField(model.emailController, label: "Email Address", hint: "e.g example@gmail.com", textInputType: TextInputType.emailAddress),
                new Padding(padding: EdgeInsets.only(top: 20)),
                RoutesTextField(model.passwordController, label: "Password", hint: "Choose a password", obscureText: true,),
                new Padding(padding: EdgeInsets.only(top: 10)),
                Text("Password must not be less than 6 characters", style: TextStyle(fontSize: 12, color: Theme.of(context).accentColor),),
                new Padding(padding: EdgeInsets.only(top: 33)),
                Button("Sign Up", ()async {
                  var response = await model.signUp(view.args.phoneNumber, view.args.otpAuthenticationId);
                  if (response != null){
                    Navigator.pushReplacementNamed(context, "signup_success");
                  }

                }, disabled: !model.inputValid,),
                new Padding(padding: EdgeInsets.only(top: 27)),
                new Center(
                  child: new RichText(
                    textAlign: TextAlign.center,
                    text: new TextSpan(
                      children: [
                        new TextSpan(
                          text: 'By signing up, you agree to our\n',
                          style: Theme.of(context).textTheme.body1,
                        ),
                        new TextSpan(
                          text: 'Terms of Service',
                          style: new TextStyle(color: Theme.of(context).textTheme.title.color, fontWeight: FontWeight.bold,),
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () {

                            },
                        ),
                        new TextSpan(
                          text: ' and ',
                          style: Theme.of(context).textTheme.body1,
                        ),
                        new TextSpan(
                          text: 'Privacy Policy',
                          style:  new TextStyle(color: Theme.of(context).textTheme.title.color, fontWeight: FontWeight.bold),
                          recognizer: new TapGestureRecognizer()
                            ..onTap = () {

                            },
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}