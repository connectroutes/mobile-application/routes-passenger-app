

import 'package:flutter/material.dart';
import 'package:routes_app_lib/routes_app_lib.dart';

class SignUpSuccess extends StatelessWidget{


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: 94, left: 26, right: 24),
        child: Column(
          children: <Widget>[
            Text("Your account has been created!", style: Theme.of(context).textTheme.display1),
            new Padding(padding: EdgeInsets.only(top: 48)),
            Button("Continue", (){
              Navigator.of(context).pushReplacementNamed("home");
            })
          ],
        ),
      ),
    );
  }
}