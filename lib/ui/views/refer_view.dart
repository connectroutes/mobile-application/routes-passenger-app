import 'package:flutter/material.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_passenger_app/core/viewmodels/promo_model.dart';
import 'package:routes_passenger_app/core/viewmodels/refer_model.dart';
import 'package:routes_passenger_app/ui/views/base_view.dart';

class ReferView extends StatefulWidget {
  @override
  _ReferViewState createState() => _ReferViewState();
}

class _ReferViewState extends State<ReferView> {
  @override
  Widget build(BuildContext context) {
    return BaseView<ReferModel>(
      builder: (context, model, child) => Scaffold(
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(
              top: MediaQuery.of(context).padding.top,
            ),
            padding: EdgeInsets.only(left: 22, top: 10, right: 22),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                RoutesAppBar(
                  "Refer a Friend",
                ),
                Padding(
                  padding: EdgeInsets.only(top: 45),
                ),
                Text(
                  "Want a free ride?",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 12),
                ),
                Text("Refer a friend and get N500 off your next ride"),
                Padding(
                  padding: EdgeInsets.only(top: 30),
                ),
                Center(
                  child: new Image.asset(
                    "assets/refer_friend.png",
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 33),
                ),
                Stack(
                  children: <Widget>[
                    RoutesTextField(
                      model.referrerCodeController,
                      label: "Your Invite Code",
                      hint: "Enter Promo Code",
                      readOnly: true,
                      dottedBorder: true,
                      height: 80,
                      textAlign: TextAlign.center,
                      hintFontWeight: FontWeight.bold,
                      textStyle: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Positioned(
                      child: GestureDetector(
                          onTap: () {
                            Utilities.copyToClipboard(model.referrerCodeController.text);
                            Utilities.showToast(context, "Invite code copied");
                          },
                          child: Text(
                            "Copy",
                            style: TextStyle(
                                color: Color(0xff6D9933),
                                fontWeight: FontWeight.w500),
                          )),
                      right: 14,
                      bottom: 12,
                    )
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: 33,
                  ),
                ),
                Button(
                  "Refer a Friend",
                  () {},
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
